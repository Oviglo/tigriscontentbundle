import TigrisContentEditor from '../vue/TigrisContentEditor.vue'
import ImageWidget from '../vue/ImageWidget.vue'
vueApp.component('tigris-content-editor', TigrisContentEditor);
vueApp.component('image-widget', ImageWidget);