
import TigrisEvents from "../../../tigris-base-bundle/assets/js/tigris-events";
document.addEventListener(TigrisEvents.FORM_LOADED, function () {
    let pageTypeSelect = document.getElementById("page_type");
    if (null == pageTypeSelect) {
        return;
    }
    let pageTemplateSelect = document.getElementById("page_template").closest(".form-group");
    let pageContentTypeSelect = document.getElementById("page_contentType").closest(".form-group");
    let pageUrlInput = document.getElementById("page_url").closest(".form-group");

    function changePageType() {
        pageUrlInput.classList.add('d-none');
        pageTemplateSelect.classList.add('d-none');
        pageContentTypeSelect.classList.add('d-none');
        switch (pageTypeSelect.value) {
            case 'url':
                pageUrlInput.classList.remove('d-none');
                break;
            case 'text':
                pageTemplateSelect.classList.remove('d-none');
                break;
            case 'content_list':
                pageContentTypeSelect.classList.remove('d-none');
                break;
        }
    }

    if (null != pageTypeSelect) {
        changePageType();
        pageTypeSelect.addEventListener("change", function () {
            changePageType();
        });
    }
});
