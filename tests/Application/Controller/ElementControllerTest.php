<?php

namespace Tigris\ContentBundle\Tests\Application\Controller;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;
use Tigris\ContentBundle\Controller\ElementController;
use Tigris\ContentBundle\Repository\ElementRepository;

#[CoversClass(ElementController::class)]
class ElementControllerTest extends AbstractLoginWebTestCase
{
    #[Test]
    public function showBySlug(): void
    {
        $this->loginAdmin();

        $element = self::getContainer()->get(ElementRepository::class)->findOneBy([]);

        $this->client->request('GET', '/element/'.$element->getSlug());

        self::assertResponseIsSuccessful();
        self::assertSelectorTextContains('h1', $element->getName());
    }

    #[Test]
    public function showByWrongSlug(): void
    {
        $this->loginAdmin();

        $this->client->request('GET', '/element/wrong-slug');

        self::assertResponseStatusCodeSame(404);
    }
}
