<?php

namespace Tigris\ContentBundle\Tests\Integration\Entity;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Tests\Helper\EntityTestTrait;
use Tigris\ContentBundle\Entity\Page;

#[CoversClass(Page::class)]
class PageTest extends KernelTestCase
{
    use EntityTestTrait;

    private static function getEntity(): Page
    {
        $entity = new Page();
        $entity
            ->setSlug('home')
            ->setName('Home')
            ->setDescription('Welcome')
            ->setTemplate('default')
        ;

        return $entity;
    }

    #[Test]
    public function testValidEntity(): void
    {
        $entity = self::getEntity();

        $this->validateEntity($entity, 0);
    }

    #[Test]
    public function testGetters(): void
    {
        $entity = self::getEntity();

        self::assertEquals('home', $entity->getSlug());
        self::assertEquals('Home', $entity->getName());
        self::assertEquals('Welcome', $entity->getDescription());
        self::assertEquals('default', $entity->getTemplate());
    }
}
