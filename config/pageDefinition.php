<?php

use Symfony\Component\Config\Definition\Configurator\DefinitionConfigurator;
use Tigris\BaseBundle\Form\ConfiguredForm\Definition;

return static function (DefinitionConfigurator $definition): void {
    Definition::set($definition->rootNode()
        ->children()
        ->arrayNode('page_templates')
            ->useAttributeAsKey('id')
            ->arrayPrototype()
                ->children()
                ->scalarNode('name')->end()
                ->scalarNode('view')->end()
                ->arrayNode('content_options')
                    ->children()
                    ) // \Definition::set
                    ->end()
                ->end()
            ->end()
        ->end()
    ->end()
    ;
};