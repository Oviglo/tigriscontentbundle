<?php

use Symfony\Component\Config\Definition\Configurator\DefinitionConfigurator;
use Tigris\BaseBundle\Form\ConfiguredForm\Definition;

return static function (DefinitionConfigurator $definition): void {
    Definition::set($definition->rootNode()
        ->children()
        ->arrayNode('elements')
                ->useAttributeAsKey('id')
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('name')->end()
                            ->scalarNode('description')->end()
                            ->booleanNode('categories')->defaultFalse()->end()
                            ->integerNode('count_per_page')->min(1)->defaultValue(10)->end()
                            ->scalarNode('sort_by')->defaultValue('id')->end()
                            ->booleanNode('order_desc')->defaultTrue()->end()
                            ->booleanNode('allow_user_adding')->defaultFalse()->end()
                            ->booleanNode('published_date')->defaultFalse()->end()
                            ->booleanNode('unpublished_date')->defaultFalse()->end()
                            ->booleanNode('sortable')->defaultFalse()->end()
                            ->booleanNode('enable_view_count')->defaultFalse()->end()
                            ->booleanNode('enable_ai')->defaultFalse()->end()

                            ->arrayNode('social')
                                ->children()
                                    ->arrayNode('reactions')
                                        ->useAttributeAsKey('name')
                                        ->arrayPrototype()
                                            ->children()
                                            ->scalarNode('label')->end()
                                            ->scalarNode('type')->end()
                                            ->arrayNode('data')->end()
                                            ->scalarNode('visibility')->end()
                                            ->end()
                                        ->end()
                                    ->end()

                                    ->arrayNode('comments')
                                        ->children()
                                        ->scalarNode('label')->end()
                                        ->scalarNode('visibility')->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ) // \Definition::set



                        ->end()
                    ->end()
                ->end()
            ->end()
            ;
};