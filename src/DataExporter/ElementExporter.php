<?php

namespace Tigris\ContentBundle\DataExporter;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\DataExporter\AbstractExporter;
use Tigris\ContentBundle\Repository\ElementRepository;

class ElementExporter extends AbstractExporter
{
    private array $configurations;

    public function __construct(EntityManagerInterface $entityManager, array $configurations, TranslatorInterface $translator, private readonly ElementRepository $elementRepository)
    {
        parent::__construct($entityManager, $configurations, $translator);
        $this->configurations = $configurations;
    }

    public function format(array $options = [], string $writerName = null): array
    {
        if (!isset($this->configurations[$this->name])) {
            throw new \Exception("Content type $this->name doesn't exist");
        }
        $elementConfig = $this->configurations[$this->name];

        $entities = $this->elementRepository->findByType($this->name);
        $data = [];
        $header = [];
        $header[] = ['value' => 'name'];

        foreach ($elementConfig['fields'] as $name => $field) {
            $header[] = ['value' => $name];
        }

        $data[] = $header;

        foreach ($entities as $entity) {
            $row = [];
            $row[] = ['value' => $entity->getName()];

            foreach ($elementConfig['fields'] as $name => $field) {
                $fn = 'get'.ucfirst((string) $name);
                $row[] = ['value' => $entity->$fn()];
            }

            $data[] = $row;
        }

        return $data;
    }
}
