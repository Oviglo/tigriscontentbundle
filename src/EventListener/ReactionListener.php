<?php

namespace Tigris\ContentBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Tigris\ContentBundle\Entity\Element;
use Tigris\ContentBundle\Repository\ElementRepository;
use Tigris\SocialBundle\Event\ReactionEvents;

class ReactionListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [];
        if (class_exists(ReactionEvents::class)) {
            return [
                ReactionEvents::REACTION_ADDED => 'onReactionAdded',
            ];
        } else {
            return [];
        }
    }

    public function __construct(private readonly ElementRepository $elementRepository)
    {
    }

    public function onReactionAdded(GenericEvent $event): void
    {
        $reaction = $event->getSubject();

        if (Element::class == $reaction->getEntityName()) {
            $element = $this->elementRepository->findOneById($reaction->getEntityId());

            if (!$element instanceof Element) {
                return;
            }
            $data = $reaction->getData();
            $data['route'] = [
                'name' => 'tigris_content_element_show',
                'params' => ['type' => $element->getType(), 'slug' => $element->getSlug()],
            ];

            $reaction->setData($data);
        }
    }
}
