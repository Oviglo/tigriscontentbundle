<?php

namespace Tigris\ContentBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Entity\DataImport;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\FormEvent;

class DataImporterListener implements EventSubscriberInterface
{
    public function __construct(private readonly array $contentElements, private readonly TranslatorInterface $translator)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            Events::LOAD_DATA_IMPORTER_FORM => 'onLoadForm',
        ];
    }

    public function onLoadForm(FormEvent $event): void
    {
        $formData = $event->getFormData();
        if (!$formData instanceof DataImport || $formData->getType() != 'element') {
            return;
        }
        $choices = [];
        foreach ($this->contentElements as $key => $element) {
            $choices[$this->translator->trans($element['name'])] = $key;
        }

        $event->getForm()->get('options')->add('elementType', ChoiceType::class, [
            'label' => '',
            'choices' => $choices,
        ]);

        $event->getForm()->get('options')->add('defaultPublished', CheckboxType::class, [
            'label' => 'data.importer.options.default_published',
            'required' => false,
            'data' => true,
        ]);
    }
}
