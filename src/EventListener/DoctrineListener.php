<?php

namespace Tigris\ContentBundle\EventListener;

use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Event\PostLoadEventArgs;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\ContentBundle\Entity\Category;
use Tigris\ContentBundle\Entity\Element;
use Tigris\ContentBundle\Entity\Page;

#[AsDoctrineListener(event: Events::postLoad)]
#[AsDoctrineListener(event: Events::prePersist)]
class DoctrineListener
{
    public function __construct(
        private readonly array $elementTypes,
        private readonly array $pageTemplates,
        private readonly array $menus,
        private readonly RouterInterface $router,
        private readonly TranslatorInterface $translator
    ) {
    }

    public function postLoad(PostLoadEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof Element) {
            $entity->setTypeLabel($this->translator->trans($this->elementTypes[$entity->getType()]['name']));
            $entity->setIsCategories($this->elementTypes[$entity->getType()]['categories']);
        }

        if ($entity instanceof Page) {
            $entity->setTemplateLabel($this->translator->trans($this->pageTemplates[$entity->getTemplate()]['name']));
            $entity->setHasContentOptions(!empty($this->pageTemplates[$entity->getTemplate()]['content_options']));
            $menuLabels = [];

            foreach ($entity->getMenus() as $menu) {
                $menuLabels[$menu] = $this->translator->trans($this->menus[$menu]['name']);
            }

            $entity->setMenuLabels($menuLabels);
        }

        if ($entity instanceof Category) {
            $typeLabels = [];
            foreach ($entity->getTypes() as $key => $type) {
                $typeLabels[$type] = $this->translator->trans('content.element.type.' . $type);
            }

            $entity->setTypesLabels($typeLabels);
        }

        if (
            (\Tigris\SocialBundle\Entity\Reaction::class == $entity::class
                || \Tigris\SocialBundle\Entity\Comment::class == $entity::class)
            && \Tigris\ContentBundle\Entity\Element::class == $entity->getEntityName()
        ) {
            if (empty($entity->getEntityTitle())) {
                $element = $args->getObjectManager()->getRepository(Element::class)->findOneById($entity->getEntityId());
                if (!is_null($element)) {
                    $entity->setEntityTitle($element->getName());
                    $args->getObjectManager()->persist($entity);
                    $args->getObjectManager()->flush();
                }
            }

            $url = $this->router->generate('tigris_content_element_show_id', ['id' => $entity->getEntityId()]);
            $entity->setEntityUrl($url);
        }
    }

    public function prePersist(PrePersistEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof Element && null !== $entity->getSimpledata()) {
            $entity->setData($entity->getSimpledata());
        }
    }
}
