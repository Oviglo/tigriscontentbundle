<?php

namespace Tigris\ContentBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Dashboard\DashboardCard;
use Tigris\BaseBundle\Dashboard\DashboardCardListItem;
use Tigris\BaseBundle\Event\DashboardEvent;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\FormEvent;
use Tigris\BaseBundle\Event\SearchEvent;
use Tigris\BaseBundle\Event\SitemapEvent;
use Tigris\BaseBundle\Event\StatsEvent;
use Tigris\BaseBundle\Search\SearchResult;
use Tigris\BaseBundle\Utils\Sitemap\Url;
use Tigris\BaseBundle\Utils\Stats;
use Tigris\ContentBundle\Manager\ElementManager;
use Tigris\ContentBundle\Repository\ElementRepository;
use Tigris\ContentBundle\Repository\PageRepository;

class BaseListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            Events::LOAD_STATS => 'onLoadStats',
            Events::SEARCH => 'onSearch',
            Events::LOAD_SEARCH_OPTIONS => 'onLoadSearchOptions',
            Events::GENERATE_SITEMAP => 'onGenerateSitemap',
            Events::LOAD_ADMIN_DASHBOARD => 'onAdminDashboard',
        ];
    }

    public function __construct(
        private readonly ElementRepository $elementRepository,
        private readonly PageRepository $pageRepository,
        private readonly RouterInterface $router,
        private readonly TranslatorInterface $translator,
        private readonly ElementManager $elementManager
    ) {
    }

    public function onLoadStats(StatsEvent $event): void
    {
        $event->addStats(new Stats('menu.content.content', 'tigris_content_admin_element_chart'));
    }

    public function onSearch(SearchEvent $event): void
    {
        $search = $event->getSearch();
        $options = $event->getOptions();

        if (empty($search) || (!in_array('all', $options) && !in_array('content', $options))) {
            return;
        }

        $results = $this->elementManager->search($search);

        foreach ($results as $result) {
            $image = null;
            if (null != $result->getImage()) {
                $image = $result->getImage()->getWebPath('thumbnail');
            }
            $event->addResult(new SearchResult($result->getName(), substr(strip_tags((string) $result->getContent()), 0, 250), 'tigris_content_element_show', ['type' => $result->getType(), 'slug' => $result->getSlug()], $image));
        }
    }

    public function onLoadSearchOptions(GenericEvent $event): void
    {
        $options = $event->getArguments();

        $options['content'] = 'search.option.content';

        $event->setArguments($options);
    }

    public function onLoadConfigs(FormEvent $event): void
    {
        // $form = $event->getForm();
        // $form->add('TigrisContentBundle', ConfigType::class, array(
        //     'label' => 'config.content',
        // ));

        // $event->setForm($form);
    }

    public function onGenerateSitemap(SitemapEvent $event): void
    {
        $pages = $this->pageRepository->findPublicPages();

        foreach ($pages as $page) {
            $url = (new Url())
                ->setLoc($page->getPath($this->router, true))
                ->setLastmod($page->getUpdatedAt());

            $event->addUrl($url);
        }

        $elements = $this->elementRepository->findAllPublic();

        foreach ($elements as $element) {
            $url = (new Url())
                ->setLoc($this->router->generate('tigris_content_element_show', ['type' => $element->getType(), 'slug' => $element->getSlug()], RouterInterface::ABSOLUTE_URL))
                ->setLastmod($element->getUpdatedAt());

            $event->addUrl($url);
        }
    }

    public function onAdminDashboard(DashboardEvent $event): void
    {
        $elements = $this->elementRepository->findByViews();
        if ([] !== $elements) {
            $card = (new DashboardCard('content'))
                ->setTitle('dashboard.content.element_viewed')
                ->setId('more-viewed-contents')
                ->setType(DashboardCard::TYPE_LIST);

            foreach ($elements as $element) {
                $card->addListItem(new DashboardCardListItem($element->getName(), $this->translator->trans('dashboard.content.views', ['views' => $element->getViewCount()])));
            }
            $event->addCard($card);
        }
    }
}
