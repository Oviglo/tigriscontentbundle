<?php

namespace Tigris\ContentBundle\EventListener;

use Tigris\BaseBundle\Event\MenuEvent;

class MenuListener
{
    public function __construct(private readonly array $elementTypes)
    {
    }

    public function onLoadAdminMenu(MenuEvent $event)
    {
        $menu = $event->getMenu();

        $parent = $menu->addChild('menu.content.content', [
            'uri' => '#content',
        ]);

        $parent->addChild('menu.content.pages', [
            'route' => 'tigris_content_admin_page_index',
        ]);

        /*$parent->addChild('menu.content.elements', array(
            'route' => 'tigris_content_admin_element_index'
        ));*/

        foreach ($this->elementTypes as $type => $elementType) {
            $parent->addChild($elementType['name'].'_menu', [
                'route' => 'tigris_content_admin_element_index',
                'routeParameters' => ['type' => $type],
            ]);
        }

        $parent->addChild('menu.content.categories', [
            'route' => 'tigris_content_admin_category_index',
        ]);
    }
}
