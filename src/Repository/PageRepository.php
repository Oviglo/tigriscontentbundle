<?php

namespace Tigris\ContentBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Tigris\BaseBundle\Traits\RepositoryTrait;
use Tigris\ContentBundle\Entity\Page;

/**
 * @author Loïc Ovigne <ovigne.loic@gmail.com>
 */
class PageRepository extends NestedTreeRepository implements ServiceEntityRepositoryInterface
{
    use RepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        $manager = $registry->getManagerForClass(Page::class);

        parent::__construct($manager, $manager->getClassMetadata(Page::class));
    }

    public function findData(array $criteria, array $filters = []): Paginator
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->addOrderBy('e.left', 'ASC')
            ->addOrderBy('e.position', 'ASC')
        ;

        $criteria['order'] = 'left';
        $criteria['rev'] = false;

        $this->addBasicCriteria($queryBuilder, $criteria);

        if ('' != $filters['public']) {
            $queryBuilder
                ->andWhere('e.public = :public')
                ->setParameter(':public', $filters['public'])
            ;
        }

        if ('' != $filters['menu']) {
            $queryBuilder
                ->andWhere('e.menus LIKE :menu')
                ->setParameter(':menu', '%'.$filters['menu'].'%')
            ;
        }

        if ('' != $filters['template']) {
            $queryBuilder
                ->andWhere('e.template = :template')
                ->setParameter(':template', $filters['template'])
            ;
        }

        return new Paginator($queryBuilder, true);
    }

    public function findHomePage(): ?Page
    {
        $queryBuilder = $this->createQueryBuilder('p')
            ->andWhere('p.public = :public')
            ->setParameter(':public', true)
            ->orderBy('p.left', 'ASC')
            ->setMaxResults(1)
        ;

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function getMenuTree($name)
    {
        $queryBuilder = $this->getNodesHierarchyQueryBuilder();
        $queryBuilder
            ->addSelect('c, cc')
            ->leftJoin('node.children', 'c')
            ->leftJoin('c.children', 'cc')
            ->andWhere('node.public = :public')
            ->andWhere('node.menus LIKE :menu')
            ->andWhere('node.parent IS NULL')
            ->orderBy('node.position', 'ASC')
            ->setParameter(':menu', '%'.$name.'%')
            ->setParameter(':public', true);

        return $queryBuilder->getQuery()->getResult();
    }

    public function getTree()
    {
        $queryBuilder = $this->getRootNodesQueryBuilder();
        $queryBuilder
            ->addSelect('c, cc')
            ->leftJoin('node.children', 'c')
            ->leftJoin('c.children', 'cc')
            ->addOrderBy('node.position', 'ASC')
        ;

        return $queryBuilder->getQuery()->getResult();
    }

    public function findPublicPages(): array
    {
        $queryBuilder = $this->createQueryBuilder('p');
        $queryBuilder
            ->where('p.public = true')
            ->andWhere('p.menus IS NOT NULL')
            ->andWhere('p.accessAuthenticated = false')
            ->andWhere('p.nofollow != true')
            ->addOrderBy('p.position', 'ASC')
        ;

        return $queryBuilder->getQuery()->getResult();
    }

    public function findAllByMenu(string $menu): array
    {
        $queryBuilder = $this->createQueryBuilder('p');
        $queryBuilder
            ->where('p.menus LIKE :menu')
            ->andWhere('p.public = true')
            ->andWhere('p.accessAuthenticated = false')
            ->addOrderBy('p.position', 'ASC')
            ->setParameter(':menu', '%'.$menu.'%')
        ;

        return $queryBuilder->getQuery()->getResult();
    }
}
