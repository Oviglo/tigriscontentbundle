<?php

namespace Tigris\ContentBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Tigris\BaseBundle\Traits\RepositoryTrait;
use Tigris\ContentBundle\Entity\Category;

/**
 * @author Loïc Ovigne <ovigne.loic@gmail.com>
 */
class CategoryRepository extends NestedTreeRepository implements ServiceEntityRepositoryInterface
{
    use RepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        $manager = $registry->getManagerForClass(Category::class);

        parent::__construct($manager, $manager->getClassMetadata(Category::class));
    }

    public function findData(array $criteria = []): Paginator
    {
        $queryBuilder = $this->createQueryBuilder('e');

        $criteria['order'] = 'left';
        $criteria['rev'] = false;

        $this->addBasicCriteria($queryBuilder, $criteria);
        $queryBuilder->addOrderBy('e.position', 'ASC');

        if (!empty($criteria['type'])) {
            $queryBuilder->andWhere('e.types LIKE :type')
                ->setParameter(':type', '%'.$criteria['type'].'%');
        }

        if (null !== $criteria['public']) {

            $queryBuilder
                ->andWhere('e.public = :public')
                ->setParameter(':public', $criteria['public'])
            ;
        }

        if (array_key_exists('parent', $criteria)) {
            if ($criteria['parent'] instanceof Category) {
                $queryBuilder
                    ->andWhere('e.parent = :parent')
                    ->setParameter(':parent', $criteria['parent'])
                ;
            } else {
                $queryBuilder
                    ->andWhere('e.parent IS NULL')
                ;
            }
        }

        return new Paginator($queryBuilder, true);
    }

    public function getMenuTree($elementType = null)
    {
        $queryBuilder = $this->getNodesHierarchyQueryBuilder();
        $queryBuilder
            ->addSelect('children')
            // ->addSelect('e') // Performance !
            ->leftJoin('node.children', 'children', 'WITH', 'children.public = :public')
            ->leftJoin('node.elements', 'e', 'WITH', 'e.public = :public')
            ->andWhere('node.public = :public')
            ->andWhere('node.parent IS NULL')
            ->addOrderBy('node.position', 'ASC')
            ->setParameter(':public', true)
        ;

        if ($elementType) {
            $queryBuilder->andWhere('node.types LIKE :type')
                ->setParameter(':type', '%'.$elementType.'%')
            ;
        }

        return $queryBuilder->getQuery()->getResult();
    }

    public function getTree()
    {
        $queryBuilder = $this->getRootNodesQueryBuilder();
        $queryBuilder
            ->addSelect('c, cc')
            ->leftJoin('node.children', 'c')
            ->leftJoin('c.children', 'cc')
            ->addOrderBy('node.position', 'ASC')
        ;

        return $queryBuilder->getQuery()->getResult();
    }
}
