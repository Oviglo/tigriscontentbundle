<?php

namespace Tigris\ContentBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Tigris\BaseBundle\Event\QueryEvent;
use Tigris\BaseBundle\Traits\RepositoryTrait;
use Tigris\ContentBundle\Entity\Category;
use Tigris\ContentBundle\Entity\Element;

/**
 * @author Loïc Ovigne <ovigne.loic@gmail.com>
 */
class ElementRepository extends ServiceEntityRepository
{
    use RepositoryTrait;

    public function __construct(ManagerRegistry $registry, private readonly EventDispatcherInterface $eventDispatcher, private readonly array $elementsConfig)
    {
        parent::__construct($registry, Element::class);
    }

    public function findData(array $criteria, array $filters = []): Paginator
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->addSelect('c, u, t, g')
            ->leftJoin('e.categories', 'c')
            ->leftJoin('e.user', 'u')
            ->leftJoin('u.groups', 'g')
            ->leftJoin('e.tags', 't')
        ;

        if (isset($filters['public']) && '' != $filters['public']) {
            $queryBuilder
                ->andWhere('e.public = :public')
                ->setParameter(':public', $filters['public'])
            ;
        }

        if (isset($filters['user']) && '' != $filters['user']) {
            $queryBuilder
                ->andWhere('e.user = :user')
                ->setParameter(':user', $filters['user'])
            ;
        }

        if ('' != $filters['type']) {
            $queryBuilder
                ->andWhere('e.type = :type')
                ->setParameter(':type', $filters['type'])
            ;
        }

        if (isset($filters['search']) && $filters['search']) {
            $queryBuilder->andWhere("e.name LIKE '".$filters['search']."%'");
        }

        if ('user' == $criteria['order']) {
            $queryBuilder->orderBy('u.username', ($criteria['rev']) ? 'DESC' : 'ASC');
        } else {
            $queryBuilder->orderBy('e.'.$criteria['order'], ($criteria['rev']) ? 'DESC' : 'ASC');
        }

        $queryBuilder->setFirstResult($criteria['begin']);

        if ($criteria['count'] > 0) {
            $queryBuilder->setMaxResults($criteria['count']);
        }

        return new Paginator($queryBuilder);
    }

    public function findByType(string $type, int|array $criteria = [], int $begin = 0, string $order = 'createdAt', bool $desc = true, array $categories = []): Paginator
    {
        if (is_int($criteria)) {
            $criteria = [
                'count' => $criteria,
                'begin' => $begin,
                'order' => $order,
                'rev' => $desc,
                'categories' => $categories,
            ];

            // Deperecated alert
            trigger_error('The second parameter of findByType is deprecated, please use an array criteria instead.', E_USER_DEPRECATED);
        }
        $queryBuilder = $this->createQueryBuilder('e');
        $queryBuilder
            ->addSelect('c, u')
            ->leftJoin('e.categories', 'c')
            ->leftJoin('e.user', 'u')
            ->where('e.type = :type')
            ->andWhere('e.public = :public')
            ->setParameter(':type', $type)
            ->setParameter(':public', true)
            ->andWhere('e.publishedAt < :now')
            ->andWhere(
                $queryBuilder->expr()->orX(
                    'e.unpublishedAt IS NULL',
                    'e.unpublishedAt < :now'
                )
            )
            ->setParameter(':now', new \DateTime())
        ;

        if (is_array($criteria['categories']) && !empty($criteria['categories'])) {
            $queryBuilder
                ->andWhere('c.slug IN (:categories)')
                ->setParameter(':categories', $criteria['categories'])
            ;
        }

        if (!isset($criteria['search']) && $criteria['search']) {
            $queryBuilder->andWhere('e.name LIKE :search')
                ->setParameter(':search', "%{$criteria['search']}%");
        }

        $this->addBasicCriteria($queryBuilder, $criteria);

        $event = new QueryEvent($queryBuilder);
        $this->eventDispatcher->dispatch($event, 'tigris_content.find_element_by_type');

        $queryBuilder = $event->getBuilder();

        return new Paginator($queryBuilder, true);
    }

    public function findByCategory(Category $category, int $count = 30, int $begin = 0): Paginator
    {
        $queryBuilder = $this->createQueryBuilder('e');

        $queryBuilder
            ->addSelect('c')
            ->leftJoin('e.categories', 'c')
            ->where('c = :category')
            ->andWhere('e.public = :public')
            ->setParameter(':category', $category)
            ->setParameter(':public', true)
            ->orderBy('e.publishedAt', 'DESC')
            ->andWhere('e.publishedAt < :now')
            ->andWhere($queryBuilder->expr()->orX(
                'e.unpublishedAt IS NULL',
                'e.unpublishedAt < :now'
            ))
            ->setParameter(':now', new \DateTime())
            ->setFirstResult($begin)
        ;

        if ($count > 0) {
            $queryBuilder->setMaxResults($count);
        }

        return new Paginator($queryBuilder, true);
    }

    public function findOneBySlug(string $slug): ?Element
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->addSelect('c, u')
            ->leftJoin('e.categories', 'c')
            ->leftJoin('e.user', 'u')
            ->where('e.slug = :slug')
            ->setParameter(':slug', $slug)
            ->setFirstResult(0)
        ;

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function findOneById(int $id): ?Element
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->select('e, c, u')
            ->leftJoin('e.categories', 'c')
            ->leftJoin('e.user', 'u')
            ->where('e.id = :id')
            ->setParameter(':id', $id)
            ->setFirstResult(0)
        ;

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function countByType(string $type, array $categories = []): int
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->select('COUNT(e)')
            ->leftJoin('e.categories', 'c')
            ->where('e.type = :type')
            ->andWhere('e.public = 1')
            ->setParameter(':type', $type)
        ;

        foreach ($categories as $i => $category) {
            $queryBuilder
                ->andWhere("c.name = :category_{$i}")
                ->setParameter(":category_{$i}", "%{$category}%");
        }

        return (int) $queryBuilder->getQuery()->getSingleScalarResult();
    }

    public static function getFormTypeQueryBuilder(EntityRepository $er): EntityRepository
    {
        $er->createQueryBuilder('u')
            ->select('e, c, u')
            ->leftJoin('e.categories', 'c')
            ->leftJoin('e.user', 'u')
        ;

        return $er;
    }

    public function search(string $search): array
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->leftJoin('e.categories', 'c')
            ->andWhere('e.public = :public')
            ->setParameter(':public', true)
            ->orderBy('e.publishedAt', 'DESC')
            // ->andWhere('c.accessAuthenticated = :accessAuthenticated')
            // ->setParameter(':accessAuthenticated', false)
            ->andWhere('(e.data LIKE :search OR e.name LIKE :search)')
            ->andWhere('e.publishedAt <= :now')
            ->andWhere('(e.unpublishedAt > :now OR e.unpublishedAt IS NULL)')
            ->setParameter(':search', "%{$search}%")
            ->setParameter(':now', new \DateTime())
        ;

        return $queryBuilder->getQuery()->getResult();
    }

    public function findAllPublic(): array
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->leftJoin('e.categories', 'c')
            ->andWhere('e.public = :public')
            ->setParameter(':public', true)
            ->andWhere('e.publishedAt <= :now')
            ->andWhere('(e.unpublishedAt > :now OR e.unpublishedAt IS NULL)')
            ->setParameter(':now', new \DateTime())
            ->orderBy('e.publishedAt', 'DESC')
        ;

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @return array<Element>
     */
    public function findByViews(int $count = 10): array
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->where('e.viewCount > 0')
            ->orderBy('e.viewCount', 'DESC')
            ->setMaxResults($count)
        ;

        return $queryBuilder->getQuery()->getResult();
    }
}
