<?php

namespace Tigris\ContentBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Tigris\BaseBundle\Utils\Utils;
use Tigris\ContentBundle\Entity\Category;

class CategoryFixtures extends Fixture
{
    private array $categories = [
        [
            'name' => 'Albums',
            'public' => true,
            'types' => ['article'],
            'parent' => null,
        ],
        [
            'name' => 'Personnages',
            'public' => true,
            'types' => ['article'],
            'parent' => null,
        ],
        [
            'name' => 'Films',
            'public' => true,
            'types' => ['article'],
            'parent' => null,
        ],
        [
            'name' => 'Gaulois',
            'public' => true,
            'types' => ['article'],
            'parent' => 'personnages',
        ],
        [
            'name' => 'Romains',
            'public' => true,
            'types' => ['article'],
            'parent' => 'personnages',
        ],
    ];

    public function load(ObjectManager $manager): void
    {
        foreach ($this->categories as $category) {
            $entity = (new Category())
                ->setName($category['name'])
                ->setTypes($category['types'])
                ->setPublic($category['public'])
            ;

            if (null !== $category['parent'] && $this->hasReference('content-category-'.$category['parent'], Category::class)) {
                $parent = $this->getReference('content-category-'.$category['parent'], Category::class);
                $entity->setParent($parent);
            }

            $manager->persist($entity);

            $this->addReference('content-category-'.Utils::slugify($entity->getName()), $entity);
        }

        $manager->flush();
    }
}
