<?php

namespace Tigris\ContentBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Tigris\BaseBundle\DataFixtures\UserFixtures;
use App\Entity\User;
use Tigris\ContentBundle\Entity\Element;

class ElementFixtures extends Fixture implements DependentFixtureInterface
{
    private array $elements = [
        [
            'name' => 'Asterix le Gaulois',
            'type' => 'article',
            'user' => 'Asterix',
            'public' => true,
            'publishedAt' => '1 year ago',
            'data' => [
                'content' => "En 50 av. J.-C., après la défaite de Vercingétorix, toute la Gaule est occupée. Toute ? Non, car un petit village d'Armorique résiste encore aux assauts des légions de Petibonum, Laudanum, Babaorum et Aquarium et Jules César est fort contrarié de voir une petite région mettre en échec son armée.",
            ],
        ],
        [
            'name' => 'La Serpe d\'or',
            'type' => 'article',
            'user' => 'Obelix',
            'public' => false,
            'publishedAt' => '2 months ago',
            'data' => [
                'content' => 'Dérogeant à sa légendaire sagesse, Panoramix laisse fuser dans la douce quiétude des environs du village des jurons qui ne lui ressemblent guère…',
            ],
        ],
        [
            'name' => 'Astérix et les Goths',
            'type' => 'article',
            'user' => 'Obelix',
            'public' => true,
            'publishedAt' => '2 days ago',
            'data' => [
                'content' => 'Comme chaque année, Panoramix se rend avec plaisir dans la forêt des Carnutes pour participer à la grande assemblée des druides gaulois.',
            ],
        ],
        [
            'name' => 'Astérix Gladiateur',
            'type' => 'article',
            'user' => 'Asterix',
            'public' => true,
            'publishedAt' => '20 days ago',
            'data' => [
                'content' => 'Caligula Alavacomgetepus, préfet des Gaules veut faire forte impression à Rome et décide d’offrir à César rien moins qu’un Irréductible Gaulois !',
            ],
        ],
    ];

    public function load(ObjectManager $manager): void
    {
        // $userManager = $this->container->get('fos_user.user_manager');

        foreach ($this->elements as $element) {
            $entity = (new Element())
                ->setName($element['name'])
                ->setType($element['type'])
                ->setData($element['data'])
            ;

            if (isset($element['user'])) {
                $user = $this->getReference(UserFixtures::REFERENCE.\strtolower((string) $element['user']), User::class);
                if ($user instanceof User) {
                    $entity->setUser($user);
                }
            }

            if (isset($element['public'])) {
                $entity->setPublic($element['public']);
            } else {
                $entity->setPublic(true);
            }

            if (!empty($element['date'])) {
                $entity->setCreatedAt(new \DateTime($element['date']));
            }

            if (!empty($element['publishedAt'])) {
                $entity->setPublishedAt(new \DateTime($element['publishedAt']));
            }

            $manager->persist($entity);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
        ];
    }
}
