<?php

namespace Tigris\ContentBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\ContentBundle\Entity\Page;

class PageFixtures extends Fixture
{
    private array $pages = [
        [
            'name' => 'Home',
            'public' => true,
            'user' => 'Asterix',
            'template' => 'home',
            'data' => [
                'mainText' => 'Welcome to my homepage',
            ],
            'menus' => ['main'],
        ],

        [
            'name' => 'Camps Romains',
            'public' => true,
            'user' => 'Oviglo',
            'template' => 'base',
            'data' => [
                'mainText' => 'Liste des camps Romains',
            ],
            'menus' => ['main'],
        ],

        [
            'name' => 'Aquarium',
            'public' => true,
            'user' => 'Oviglo',
            'template' => 'base',
            'data' => [
                'mainText' => "Se situe au sud-ouest du village d'après la carte.",
            ],
            'menus' => [],
            'parent' => 'Camps Romains',
        ],

        [
            'name' => 'Laudanum',
            'public' => false,
            'user' => 'Obelix',
            'template' => 'base',
            'data' => [
                'mainText' => 'La seule histoire où ce camp et ses occupants jouent un rôle central est Le Cadeau de César.',
            ],
            'menus' => ['main'],
            'parent' => 'Camps Romains',
        ],

        [
            'name' => 'Babaorum',
            'public' => true,
            'user' => 'Obelix',
            'template' => 'base',
            'data' => [
                'mainText' => "Se situe au sud du village d'après la carte.",
            ],
            'menus' => ['main'],
            'parent' => 'Camps Romains',
        ],
    ];

    public function load(ObjectManager $manager): void
    {
        // $userManager = $this->container->get('fos_user.user_manager');
        $addedPages = [];

        foreach ($this->pages as $page) {
            $entity = (new Page())
                ->setName($page['name'])
                ->setTemplate($page['template'])
                ->setData($page['data'])
                ->setMenus($page['menus'])
            ;

            /*$user = $userManager->findByUsername($page['user']);

            if ($user instanceof User) {
                $entity->setUser($user);
            }*/

            if (isset($page['public'])) {
                $entity->setPublic($page['public']);
            } else {
                $entity->setPublic(true);
            }

            if (!empty($page['date'])) {
                $entity->setCreatedAt(new \DateTime($page['date']));
            }

            if (isset($page['parent']) && isset($addedPages[$page['parent']])) {
                $entity->setParent($addedPages[$page['parent']]);
            }

            $manager->persist($entity);
            $addedPages[$entity->getName()] = $entity;
        }

        $manager->flush();
    }
}
