<?php

namespace Tigris\ContentBundle\Entity\Translation;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractTranslation;
use Gedmo\Translatable\Entity\Repository\TranslationRepository;

#[ORM\Table(name: 'content_element_translations')]
#[ORM\Index(name: 'content_element_translation_idx', columns: ['locale', 'object_class', 'field', 'foreign_key'])]
#[ORM\Entity(repositoryClass: TranslationRepository::class)]
class ElementTranslation extends AbstractTranslation
{
    /*
     * All required columns are mapped through inherited superclass
     */
}
