<?php

namespace Tigris\ContentBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\ContentBundle\Entity\Translation\ElementTranslation;
use Tigris\ContentBundle\Repository\ElementRepository;

#[ORM\Entity(repositoryClass: ElementRepository::class)]
#[ORM\Table(name: 'content_element')]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', timeAware: false, hardDelete: true)]
#[Gedmo\Loggable]
#[Gedmo\TranslationEntity(class: ElementTranslation::class)]
class Element implements \Stringable
{
    use SoftDeleteableEntity;
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int|null $id = null;

    #[ORM\Column(length: 150)]
    #[Gedmo\Translatable]
    #[Gedmo\Versioned]
    private string $name;

    #[ORM\Column]
    private bool $public = false;

    #[ORM\Column]
    private bool $draft = false;

    #[ORM\Column(unique: true)]
    #[Gedmo\Translatable]
    #[Gedmo\Slug(fields: ['createdAt', 'name'], updatable: true)]
    private string $slug;

    #[Gedmo\Locale]
    private string|null $locale = null;

    #[ORM\Column(length: 180)]
    private string $type;

    private string $typeLabel;

    #[ORM\Column(type: Types::JSON)]
    #[Gedmo\Translatable]
    #[Gedmo\Versioned]
    private array $data = [];
    private array $simpleData = [];

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(name: 'user_id', onDelete: 'SET NULL')]
    private null|User $user = null;

    #[ORM\ManyToMany(targetEntity: Category::class, inversedBy: 'elements')]
    #[ORM\JoinTable(name: 'content_element_category')]
    #[ORM\JoinColumn(name: 'element_id')]
    #[ORM\InverseJoinColumn(name: 'category_id')]
    #[ORM\OrderBy(['left' => 'ASC'])]
    private Collection $categories;

    private bool $isCategories = false;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private \DateTime|null $publishedAt;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private \DateTime|null $unpublishedAt = null;

    #[ORM\ManyToMany(targetEntity: Tag::class, mappedBy: 'elements')]
    private Collection $tags;

    #[ORM\Column]
    private int $position = 0;

    #[ORM\Column]
    private int $viewCount = 0;

    public function __construct()
    {
        $this->publishedAt = new \DateTime();
        $this->categories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function isPublic(): bool
    {
        return $this->public ?? false;
    }

    public function setPublic(bool $public): self
    {
        $this->public = $public;

        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return array|string|null
     */
    public function getData(string $name = null)
    {
        if ($name) {
            return $this->data[$name] ?? null;
        }

        return $this->data;
    }

    public function setData(array $data = null): self
    {
        $this->data = $data;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(null|User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCategories()
    {
        return $this->categories;
    }

    public function setCategories($categories): self
    {
        $this->categories = $categories;

        return $this;
    }

    public function getIsCategories()
    {
        return $this->isCategories;
    }

    public function isCategories(): bool
    {
        return $this->isCategories;
    }

    public function setIsCategories(bool $isCategories): self
    {
        $this->isCategories = $isCategories;

        return $this;
    }

    public function isAccessAuthenticated(): bool
    {
        if ($this->isCategories) {
            foreach ($this->categories as $category) {
                if ($category->isAccessAuthenticated()) {
                    return true;
                }
            }
        }

        return false;
    }

    public function getClassName(): string
    {
        return static::class;
    }

    public function getSimpleData(string $dataName = null)
    {
        return $dataName ? ($this->simpleData($dataName) ?? '') : $this->simpleData;
    }

    public function setSimpleData($simpleData)
    {
        $this->simpleData = $simpleData;

        return $this;
    }

    public function __toString(): string
    {
        return $this->name.' ('.$this->type.')';
    }

    public function getTags()
    {
        return $this->tags;
    }

    public function setTags(ArrayCollection $tags)
    {
        $this->tags = $tags;

        return $this;
    }

    public function getTypeLabel()
    {
        return $this->typeLabel;
    }

    public function setTypeLabel(string $typeLabel)
    {
        $this->typeLabel = $typeLabel;

        return $this;
    }

    public function getContent()
    {
        return $this->data['content'] ?? $this->data['body'] ?? $this->data['description'] ?? '';
    }

    public function __call(string $name, array $arguments)
    {
        if (str_starts_with($name, 'set')) {
            $name = \lcfirst(substr($name, 3));
            $this->data[$name] = $arguments[0];
        }

        if (str_starts_with($name, 'get')) {
            $name = \lcfirst(substr($name, 3));
        }

        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }

        return null;
    }

    public function getPublishedAt(): \DateTimeInterface
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(\DateTime $publishedAt = null): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    public function getUnpublishedAt()
    {
        return $this->unpublishedAt;
    }

    public function setUnpublishedAt(\DateTime $unpublishedAt = null): self
    {
        $this->unpublishedAt = $unpublishedAt;

        return $this;
    }

    public function getDraft(): bool
    {
        return $this->draft;
    }

    public function setDraft(bool $draft): self
    {
        $this->draft = $draft;

        return $this;
    }

    public function isDraft(): bool
    {
        return $this->draft ?? false;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getPosition(): int
    {
        return $this->position;
    }

    public function getViewCount(): int
    {
        return $this->viewCount ?? 0;
    }

    public function setViewCount(int $viewCount): self
    {
        $this->viewCount = $viewCount;

        return $this;
    }

    public function addView(): self
    {
        ++$this->viewCount;

        return $this;
    }
}
