<?php

namespace Tigris\ContentBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Translatable\Translatable;
use Gedmo\Tree\Traits\NestedSetEntity;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Tigris\BaseBundle\Entity\Model\Group;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\ContentBundle\Repository\PageRepository;

#[ORM\Entity(repositoryClass: PageRepository::class)]
#[ORM\Table(name: 'content_page')]
#[Gedmo\Tree(type: 'nested')]
class Page implements Translatable, \Stringable
{
    use SoftDeleteableEntity;
    use TimestampableEntity;
    use NestedSetEntity;

    final public const TYPE_TEXT = 'text';
    final public const TYPE_URL = 'url';
    final public const TYPE_CONTENT_LIST = 'content_list';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int|null $id = null;

    #[ORM\Column(length: 180)]
    #[Gedmo\Translatable]
    #[Assert\NotBlank]
    private string $name;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $namePrefix = null;

    #[ORM\Column]
    private bool $accessAuthenticated = false;

    #[ORM\Column(unique: true)]
    #[Gedmo\Translatable]
    #[Gedmo\Slug(fields: ['name'], updatable: false)]
    #[Assert\Regex('/^[a-z0-9]+(?:-[a-z0-9]+)*$/')]
    private string $slug;

    #[ORM\Column]
    private bool $public;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Gedmo\Translatable]
    private string $description;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(name: 'user_id', onDelete: 'SET NULL')]
    private User|null $user = null;

    #[ORM\ManyToOne(inversedBy: 'children')]
    #[ORM\JoinColumn(name: 'parent_id', onDelete: 'CASCADE', nullable: true)]
    #[Gedmo\TreeParent]
    #[Gedmo\SortableGroup]
    private Page|null $parent = null;

    #[ORM\OneToMany(targetEntity: Page::class, mappedBy: 'parent')]
    #[ORM\OrderBy(['left' => 'ASC'])]
    #[JMS\Groups(['none'])]
    private Collection $children;

    #[ORM\Column(type: Types::JSON)]
    private array $menus;

    private array $menuLabels;

    #[ORM\Column]
    #[Assert\NotBlank]
    private string $template;

    #[ORM\Column]
    #[Gedmo\SortablePosition]
    private int $position = 0;

    private string $templateLabel;

    #[Gedmo\Locale]
    private ?string $locale = null;

    #[ORM\Column(options: ['default' => false], nullable: true)]
    private bool|null $nofollow = false;

    #[ORM\ManyToMany(targetEntity: Group::class)]
    #[ORM\JoinTable(name: 'content_page_group')]
    #[ORM\JoinColumn(name: 'page_id')]
    #[ORM\InverseJoinColumn(name: 'group_id')]
    private null|Collection $groups = null;

    #[ORM\Column(type: Types::JSON)]
    #[Gedmo\Translatable]
    private array|null $data = [];

    private array $simpleData = [];

    #[ORM\Column(length: 20)]
    private string $type = self::TYPE_TEXT;

    #[ORM\Column(length: 50, nullable: true)]
    private string|null $contentType = null;

    #[ORM\Column(nullable: true)]
    private string|null $url = null;

    #[ORM\Column(nullable: true)]
    private string|null $arguments = null;

    #[ORM\Column(type: Types::JSON, nullable: true)]
    private array|null $contentOptions = [];

    private bool $hasContentOptions = false;

    public function __construct()
    {
        $this->left = 0;
        $this->level = 0;
        $this->right = 0;
        $this->root = null;
        $this->children = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function getId(): int|null
    {
        return $this->id;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setPublic(bool $public): self
    {
        $this->public = $public;

        return $this;
    }

    public function getPublic(): bool
    {
        return $this->public;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function isPublic(): bool
    {
        return $this->public;
    }

    public function getParent(): ?Page
    {
        return $this->parent;
    }

    public function setParent(Page $parent = null): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function setChildren($children = null): self
    {
        $this->children = $children;

        return $this;
    }

    public function getMenus(): array
    {
        return $this->menus;
    }

    public function setMenus(array $menus): self
    {
        $this->menus = $menus;

        return $this;
    }

    public function getTemplate(): string
    {
        return $this->template;
    }

    public function setTemplate(string $template): self
    {
        $this->template = $template;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user = null): self
    {
        $this->user = $user;

        return $this;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getData(string $name = null): string|array|null
    {
        if ($name) {
            return $this->data[$name] ?? null;
        }

        return $this->data ?? null;
    }

    public function setData(array $data = null): self
    {
        $this->data = $data ?? [];

        return $this;
    }

    public function isNofollow(): bool
    {
        return $this->nofollow ?? true;
    }

    public function setNofollow(bool $nofollow): self
    {
        $this->nofollow = $nofollow;

        return $this;
    }

    public function getGroups(): Collection|null
    {
        return $this->groups;
    }

    public function setGroups(Collection $groups): self
    {
        $this->groups = $groups;

        return $this;
    }

    public function getSimpleData(): array
    {
        return $this->simpleData;
    }

    public function setSimpleData(array $simpleData): self
    {
        $this->simpleData = $simpleData;

        return $this;
    }

    public function getLeft(): ?int
    {
        return $this->left;
    }

    public function getPosition(): int
    {
        return $this->position ?? 0;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getRoot(): ?int
    {
        return $this->root;
    }

    public function setRoot($root): self
    {
        $this->root = $root;

        return $this;
    }

    public function getTemplateLabel(): string
    {
        return $this->templateLabel ?? '';
    }

    public function setTemplateLabel($templateLabel): self
    {
        $this->templateLabel = $templateLabel;

        return $this;
    }

    public function getMenuLabels(): array
    {
        return $this->menuLabels;
    }

    public function setMenuLabels(array $menuLabels): self
    {
        $this->menuLabels = $menuLabels;

        return $this;
    }

    public function getNamePrefix(): string
    {
        return $this->namePrefix ?? '';
    }

    public function setNamePrefix(string $namePrefix = null): self
    {
        $this->namePrefix = $namePrefix;

        return $this;
    }

    public function getAccessAuthenticated(): bool
    {
        return $this->isAccessAuthenticated();
    }

    public function isAccessAuthenticated(): bool
    {
        return $this->accessAuthenticated ?? false;
    }

    public function setAccessAuthenticated(bool $accessAuthenticated = null): self
    {
        $this->accessAuthenticated = $accessAuthenticated;

        return $this;
    }

    public function getContentType(): ?string
    {
        return $this->contentType;
    }

    public function setContentType(string $contentType): self
    {
        $this->contentType = $contentType;

        return $this;
    }

    public function getType(): string
    {
        return $this->type ?? self::TYPE_TEXT;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url ?? '';
    }

    public function setUrl(string $url = null): self
    {
        $this->url = $url;

        return $this;
    }

    public function getArguments(): string
    {
        return $this->arguments ?? '';
    }

    public function setArguments(string $arguments = null): self
    {
        $this->arguments = $arguments;

        return $this;
    }

    public function getPath(RouterInterface $router, bool $absoluteUrl = false): string
    {
        return match ($this->type) {
            static::TYPE_TEXT => $router->generate('tigris_content_page_show', ['slug' => $this->getSlug()], $absoluteUrl ? RouterInterface::ABSOLUTE_URL : RouterInterface::ABSOLUTE_PATH),
            static::TYPE_URL => $this->getUrl(),
            static::TYPE_CONTENT_LIST => $router->generate('tigris_content_element_list', ['type' => $this->getContentType()], $absoluteUrl ? RouterInterface::ABSOLUTE_URL : RouterInterface::ABSOLUTE_PATH),
            default => '',
        };
    }

    public function getContentOptions(): array
    {
        return $this->contentOptions ?? [];
    }

    public function setContentOptions(array $contentOptions): self
    {
        $this->contentOptions = $contentOptions;

        return $this;
    }

    public function getOptions(string $name): mixed
    {
        return $this->contentOptions[$name] ?? null;
    }

    public function setHasContentOptions(bool $hasContentOptions): self
    {
        $this->hasContentOptions = $hasContentOptions;

        return $this;
    }

    public function hasContentOptions(): bool
    {
        return $this->hasContentOptions;
    }
}
