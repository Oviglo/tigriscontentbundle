<?php

namespace Tigris\ContentBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'content_tag')]
class Tag
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int|null $id = null;

    #[ORM\Column]
    private string $name;

    #[ORM\ManyToMany(targetEntity: Element::class, inversedBy: 'tags')]
    private Collection $elements;

    public function getId(): int|null
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getElements()
    {
        return $this->elements;
    }

    public function setElements(Collection $elements): self
    {
        $this->elements = $elements;

        return $this;
    }
}
