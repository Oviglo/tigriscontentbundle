<?php

namespace Tigris\ContentBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Translatable\Translatable;
use Gedmo\Tree\Traits\NestedSetEntity;
use JMS\Serializer\Annotation as JMS;
use Tigris\BaseBundle\Entity\File;
use Tigris\ContentBundle\Repository\CategoryRepository;

#[ORM\Entity(repositoryClass: CategoryRepository::class)]
#[ORM\Table(name: 'content_category')]
#[Gedmo\Tree(type: 'nested')]
class Category implements Translatable, \Stringable
{
    use SoftDeleteableEntity;
    use TimestampableEntity;
    use NestedSetEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int|null $id = null;

    #[ORM\Column(length: 180)]
    #[Gedmo\Translatable]
    private string $name;

    #[ORM\Column(length: 180, nullable: true)]
    private string|null $namePrefix = null;

    #[ORM\Column(unique: true)]
    #[Gedmo\Slug(fields: ['name'])]
    #[Gedmo\Translatable]
    private string $slug;

    #[ORM\Column]
    private bool $public = true;

    #[Gedmo\Locale]
    private string|null $locale = null;

    #[Gedmo\TreeParent]
    #[Gedmo\SortableGroup]
    #[ORM\ManyToOne(inversedBy: 'children')]
    #[ORM\JoinColumn(name: 'parent_id', onDelete: 'CASCADE', nullable: true)]
    #[JMS\Exclude]
    private Category|null $parent = null;

    #[ORM\OneToMany(targetEntity: Category::class, mappedBy: 'parent')]
    #[ORM\OrderBy(['left' => 'ASC'])]
    #[JMS\Exclude]
    private Collection $children;

    #[ORM\ManyToMany(targetEntity: Element::class, mappedBy: 'categories')]
    #[JMS\Exclude]
    private Collection $elements;

    #[ORM\Column(type: Types::JSON)]
    private array $types = [];
    private ?array $typesLabels = null;

    #[ORM\Column]
    #[Gedmo\SortablePosition]
    private int $position = 0;

    #[ORM\Column]
    private bool $accessAuthenticated = false;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    private File|null $image = null;

    public function __construct()
    {
        $this->setTypes([]);
        $this->left = 0;
        $this->level = 0;
        $this->right = 0;
        $this->root = null;
        $this->elements = new ArrayCollection();
    }

    public function __toString(): string
    {
        return str_pad('', $this->level, '-', STR_PAD_LEFT).' '.$this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    #[JMS\VirtualProperty]
    public function getTreeName(): string
    {
        return str_pad('', $this->level, '-', STR_PAD_LEFT).' '.$this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function isPublic(): bool
    {
        return $this->public ?? false;
    }

    public function setPublic(bool $public): self
    {
        $this->public = $public;

        return $this;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRoot()
    {
        return $this->root;
    }

    public function setRoot(mixed $root): self
    {
        $this->root = $root;

        return $this;
    }

    public function getParent(): ?Category
    {
        return $this->parent;
    }

    public function setParent(Category $parent = null): self
    {
        $this->parent = $parent;
        if (is_null($parent)) {
            $this->level = 0;
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return self
     */
    public function setChildren(mixed $children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * @return array
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * @return self
     */
    public function setTypes(array $types)
    {
        $this->types = $types;

        return $this;
    }

    public function getElements(): Collection
    {
        return $this->elements;
    }

    public function setElements(Collection $elements): self
    {
        $this->elements = $elements;

        return $this;
    }

    public function getPosition(): int
    {
        return $this->position ?? 0;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getLevel(): int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getLeft(): ?int
    {
        return $this->left;
    }

    public function getTypesLabels(): array
    {
        return $this->typesLabels;
    }

    public function setTypesLabels(array $typesLabels): self
    {
        $this->typesLabels = $typesLabels;

        return $this;
    }

    public function getNamePrefix(): string
    {
        return $this->namePrefix ?? '';
    }

    public function setNamePrefix(string $namePrefix = null): self
    {
        $this->namePrefix = $namePrefix;

        return $this;
    }

    public function getAccessAuthenticated(): bool
    {
        return $this->accessAuthenticated ?? false;
    }

    public function isAccessAuthenticated(): bool
    {
        return $this->accessAuthenticated;
    }

    public function setAccessAuthenticated(bool $accessAuthenticated): self
    {
        $this->accessAuthenticated = $accessAuthenticated;

        return $this;
    }

    public function getImage(): ?File
    {
        return $this->image;
    }

    public function setImage(File $image = null): self
    {
        $this->image = $image;

        return $this;
    }
}
