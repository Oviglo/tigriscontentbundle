<?php

namespace Tigris\ContentBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\MenuEvent;

class UserProfileSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly array $elementTypes)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            Events::LOAD_MY_ACCOUNT_MENU => 'onLoadMyAccountMenu',
        ];
    }

    public function onLoadMyAccountMenu(MenuEvent $event): void
    {
        $allowUserAdding = false;

        foreach ($this->elementTypes as $elementType) {
            if ($elementType['allow_user_adding']) {
                $allowUserAdding = true;
            }
        }

        if (!$allowUserAdding) {
            return;
        }

        $event->addChild('menu.self_elements', ['route' => 'tigris_content_element_userlist']);
    }
}
