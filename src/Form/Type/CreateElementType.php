<?php

namespace Tigris\ContentBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class CreateElementType extends AbstractType
{
    public function __construct(private readonly array $elementTypes)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, [
                'label' => 'name',
            ])

        ;

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                $element = $event->getData();

                if (empty($element->getType())) {
                    $elementTypesChoices = [];
                    foreach ($this->elementTypes as $id => $type) {
                        $elementTypesChoices[$type['name']] = $id;
                    }

                    $event->getForm()->add('type', Type\ChoiceType::class, [
                        'label' => 'content.element.type.type',
                        'required' => true,
                        'expanded' => true,
                        'choices' => $elementTypesChoices,
                        'choice_label' => fn($choiceValue, $key, $value) => "element.type.{$choiceValue}.description",
                    ]);
                } else {
                    $event->getForm()->add('type', Type\HiddenType::class, []);
                }
            }
        );
    }

    public function finishView(FormView $view, FormInterface $form, array $options): void
    {
        $view->vars['multipart'] = true;
    }
}
