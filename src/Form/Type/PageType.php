<?php

namespace Tigris\ContentBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\ContentBundle\Entity\Page;

class PageType extends AbstractType
{
    public function __construct(
        private readonly array $pageTemplates,
        private readonly array $menus,
        private readonly string $groupClass,
        private readonly array $elements
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $templatesChoices = [];
        foreach ($this->pageTemplates as $id => $template) {
            $templatesChoices[$template['name']] = $id;
        }

        $menusChoices = [];
        foreach ($this->menus as $id => $menu) {
            $menusChoices[$menu['name']] = $id;
        }

        $elementChoices = [];
        foreach ($this->elements as $name => $data) {
            $elementChoices[$data['name']] = $name;
        }

        $builder
            ->add('name', TextType::class, [
                'label' => 'page.name',
            ])
            ->add('slug', TextType::class, [
                'label' => 'page.slug',
                'help' => 'page.slug_help',
            ])

            ->add('namePrefix', TextType::class, [
                'label' => 'page.name_prefix',
            ])

            ->add('accessAuthenticated', CheckboxType::class, [
                'label' => 'page.access_authenticated',
            ])

            ->add('description', TextareaType::class, [
                'label' => 'page.description',
                'required' => false,
            ])

            ->add('public', CheckboxType::class, [
                'label' => 'page.public',
                'required' => false,
            ])

            ->add('menus', ChoiceType::class, [
                'label' => 'page.menus',
                'required' => false,
                'multiple' => true,
                'choices' => $menusChoices,
            ])

            ->add('template', ChoiceType::class, [
                'label' => 'page.template.label',
                'required' => true,
                'multiple' => false,
                'choices' => $templatesChoices,
            ])

            ->add('nofollow', CheckboxType::class, [
                'label' => 'page.nofollow',
                'required' => false,
                'help' => 'page.nofollow_help',
            ])

            ->add('groups', EntityType::class, [
                'label' => 'page.groups',
                'required' => false,
                'multiple' => true,
                'class' => $this->groupClass,
            ])

            ->add('type', ChoiceType::class, [
                'label' => 'page.type.label',
                'required' => true,
                'choices' => [
                    'page.type.'.Page::TYPE_TEXT => Page::TYPE_TEXT,
                    'page.type.'.Page::TYPE_URL => Page::TYPE_URL,
                    'page.type.'.Page::TYPE_CONTENT_LIST => Page::TYPE_CONTENT_LIST,
                ],
            ])

            ->add('contentType', ChoiceType::class, [
                'label' => 'page.elementType',
                'choices' => $elementChoices,
            ])

            ->add('url', UrlType::class, [
                'label' => 'page.url',
                'required' => false,
            ])

            ->add('arguments', TextType::class, [
                'label' => 'page.arguments',
                'required' => false,
            ])
        ;

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            $this->preSetData(...)
        );
    }

    public function preSetData(FormEvent $event): void
    {
        $page = $event->getData();

        $event->getForm()->add('parent', EntityType::class, [
            'class' => Page::class,
            'multiple' => false,
            'required' => false,
            'label' => 'page.parent',
            'empty_data' => null,
            'query_builder' => function (EntityRepository $er) use ($page): QueryBuilder {
                $er = $er->createQueryBuilder('p');
                if (is_object($page) && $page->getId()) {
                    $er = $er->andWhere('p.id != '.$page->getId());
                }

                return $er->orderBy('p.left', 'ASC');
            },
            'help' => 'page.parent_help',
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'attr' => [
                'novalidate' => 'novalidate',
            ],
        ]);
    }
}
