<?php

namespace Tigris\ContentBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Tigris\ContentBundle\Entity\Category;

class ElementType extends AbstractType
{
    public function __construct(private readonly array $elementTypes, private readonly Security $security)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, [
                'label' => 'name',
            ])
            ->add('type', Type\HiddenType::class)

        ;

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                $element = $event->getData();
                $form = $event->getForm();

                if ($element->getIsCategories()) {
                    $form->add('categories', EntityType::class, [
                        'class' => Category::class,
                        'required' => false,
                        'multiple' => true,
                        'label' => false,
                        'query_builder' => function ($er) use ($element) {
                            $er = $er->createQueryBuilder('c');
                            if (is_object($element)) {
                                $er
                                    ->andWhere('c.types LIKE :type')
                                    ->setParameter(':type', '%'.$element->getType().'%')
                                ;
                            }

                            return $er->orderBy('c.left', 'ASC');
                        },
                    ]);
                }

                if ($this->security->isGranted('ROLE_ADMIN') || $this->security->isGranted('ROLE_ELEMENT_ADMIN')) {
                    $form->add('public', Type\CheckboxType::class, [
                        'label' => 'content.element.public',
                        'required' => false,
                    ]);

                    $config = $this->elementTypes[$element->getType()];
                    if ($config['published_date']) {
                        $form->add('publishedAt', Type\DateTimeType::class, [
                            'label' => 'content.element.published_date',
                            'required' => false,
                            'widget' => 'single_text',
                        ]);
                    }

                    if ($config['unpublished_date']) {
                        $form->add('unpublishedAt', Type\DateTimeType::class, [
                            'label' => 'content.element.unpublished_date',
                            'required' => false,
                            'widget' => 'single_text',
                        ]);
                    }
                } else {
                    $form->add('draft', null, [
                        'label' => 'content.element.draft',
                        'required' => false,
                        'help' => 'content.element.draft_help',
                    ]);
                }
            }
        );
    }

    public function finishView(FormView $view, FormInterface $form, array $options): void
    {
        $view->vars['multipart'] = true;
    }
}
