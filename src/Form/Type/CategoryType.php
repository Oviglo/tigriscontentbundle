<?php

namespace Tigris\ContentBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Tigris\BaseBundle\Form\Type\UploadFileType;
use Tigris\ContentBundle\Entity\Category;

class CategoryType extends AbstractType
{
    public function __construct(private readonly array $elementTypes)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, [
                'label' => 'name',
            ])

            ->add('namePrefix', Type\TextType::class, [
                'label' => 'content.category.name_prefix',
                'required' => false,
            ])

            ->add('image', UploadFileType::class, [
                'label' => 'content.category.image',
            ])

            ->add('public', Type\CheckboxType::class, [
                'label' => 'content.category.status.public',
                'required' => false,
            ])

            ->add('accessAuthenticated', Type\CheckboxType::class, [
                'label' => 'content.category.access_authenticated',
                'required' => false,
            ])
        ;

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                $category = $event->getData();

                $elementTypesChoices = [];
                foreach ($this->elementTypes as $id => $type) {
                    $elementTypesChoices[$type['name']] = $id;
                }

                $event->getForm()->add('parent', EntityType::class, [
                    'class' => Category::class,
                    'multiple' => false,
                    'required' => false,
                    'label' => 'content.category.parent',
                    'empty_data' => null,
                    'query_builder' => function ($er) use ($category) {
                        $er = $er->createQueryBuilder('c');
                        if (is_object($category) && $category->getId()) {
                            $er = $er->andWhere('c.id != '.$category->getId())
                            ->andWhere('c.root != :root')
                            ->setParameter(':root', $category);
                        }

                        return $er->orderBy('c.left', 'ASC');
                    },
                    'help' => 'content.category.parent_help',
                ]);

                $event->getForm()->add('types', Type\ChoiceType::class, [
                    'label' => 'content.category.types',
                    'required' => true,
                    'multiple' => true,
                    'choices' => $elementTypesChoices,
                    'help' => 'content.category.types_help',
                ]);
            }
        );
    }
}
