<?php

namespace Tigris\ContentBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr;
use Gedmo\Translatable\Entity\Repository\TranslationRepository;
use Gedmo\Translatable\Entity\Translation;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Tigris\BaseBundle\Manager\AbstractManager;
use Tigris\BaseBundle\Service\ArrayToFormService;
use Tigris\BaseBundle\Utils\Paginator;
use Tigris\ContentBundle\Entity\Category;
use Tigris\ContentBundle\Entity\Element;
use Tigris\ContentBundle\Repository\ElementRepository;

class ElementManager extends AbstractManager
{
    private array $externalEntities = [];

    /**
     * @var TranslationRepository
     */
    private readonly \Doctrine\ORM\EntityRepository $translationRepository;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ElementRepository $elementRepository,
        private array $elementConfig,
        private readonly EventDispatcherInterface $eventDispatcher,
        private readonly ArrayToFormService $arraytoFormService,
        private readonly RequestStack $requestStack,
    ) {
        $this->translationRepository = $em->getRepository(Translation::class);
    }

    public function findByCategory(Category $category, $count = 30, $begin = 0): Paginator
    {
        $elements = $this->elementRepository->findByCategory($category, $count, $begin);

        $paginator = new Paginator($elements);

        $this->includeExternalEntities($paginator->getIterator());

        return $paginator;
    }

    public function findByType(string $type, int|array $criteria = 30, $begin = 0, $order = 'createdAt', $desc = true, $categories = []): Paginator
    {
        if (!is_array($criteria)) {
            $criteria = ['count' => $criteria, 'begin' => $begin, 'order' => $order, 'desc' => $desc, 'categories' => $categories];

            trigger_error('The second parameter of findByType is deprecated, please use an array criteria instead.', E_USER_DEPRECATED);
        }

        $elements = $this->elementRepository->findByType($type, $criteria);

        $paginator = new Paginator($elements);

        $this->includeExternalEntities($paginator->getIterator());

        return $paginator;
    }

    public function search(string $search): array
    {
        $elements = $this->elementRepository->search($search);

        $this->includeExternalEntities($elements);

        return $elements;
    }

    public function findOneById(int $id): ?Element
    {
        $element = $this->elementRepository->findOneById($id);

        if (null !== $element) {
            $this->includeExternalEntities($element);
        }

        return $element;
    }

    public function findOneBySlug(string $slug): ?Element
    {
        $element = $this->elementRepository->findOneBySlug($slug);

        $this->includeExternalEntities($element);

        return $element;
    }

    public function includeExternalEntities($elements): void
    {
        $singleElement = false;
        if (!is_array($elements) && !$elements instanceof \Countable) {
            $elements = [$elements];
            $singleElement = true;
        }

        foreach ($elements as $element) {
            if (null === $element) {
                continue;
            }
            $element->setSimpleData($element->getData());
            $data = $element->getData();

            if (!isset($this->elementConfig[$element->getType()]) || empty($data)) {
                continue;
            }

            foreach ($this->elementConfig[$element->getType()]['fields'] as $dataFieldName => $dataFieldOptions) {
                // Field is an entity
                if (isset($data[$dataFieldName]) && isset($dataFieldOptions['class'])) {
                    $entityClass = $dataFieldOptions['class'];
                    $entityIds = $data[$dataFieldName];

                    if (isset($dataFieldOptions['multiple']) && $dataFieldOptions['multiple']) {
                        $entityIds = $data[$dataFieldName];
                    } else {
                        $entityIds = [$data[$dataFieldName]];
                    }

                    if (!isset($this->externalEntities[$entityClass])) {
                        $this->externalEntities[$entityClass] = [];
                    }
                    $this->externalEntities[$entityClass] = array_merge($this->externalEntities[$entityClass], $entityIds);
                }
            }
        }

        // Load external entities
        $externalEntitiesQueryBuilder = $this->em->createQueryBuilder();
        $index = 0;

        if ([] !== $this->externalEntities) {
            foreach ($this->externalEntities as $entityClass => $ids) {
                if (empty($ids)) {
                    continue;
                }

                $entityClassQuery = str_replace('\\', '', $entityClass);

                if (0 == $index) {
                    $externalEntitiesQueryBuilder
                        ->select("e_{$index} AS {$entityClassQuery}")
                        ->from($entityClass, "e_{$index}")
                        ->where("e_{$index}.id IN (:e_{$index}_ids)")
                        ->groupBy("{$entityClassQuery}");
                } else {
                    $externalEntitiesQueryBuilder
                        ->addSelect("e_{$index} AS {$entityClassQuery}")
                        ->leftJoin($entityClass, "e_{$index}", Expr\Join::WITH, "e_{$index}.id IN (:e_{$index}_ids)")
                        ->addGroupBy("{$entityClassQuery}");
                }
                $externalEntitiesQueryBuilder->setParameter(":e_{$index}_ids", $ids);

                ++$index;
            }

            if (0 !== $index) {
                $externalEntities = $externalEntitiesQueryBuilder->getQuery()->getResult();
                $externalEntities = $this->formatEntitiesQueryResult($externalEntities);

                foreach ($elements as $element) {
                    $this->hydrateElementData($element, $externalEntities);
                }
            }
        }

        if ($singleElement && isset($elements[0])) {
            $elements = $elements[0];
        }
    }

    private function formatEntitiesQueryResult($externalEntities)
    {
        $result = [];

        foreach ($externalEntities as $entity) {
            $keys = array_keys($entity);

            if (!isset($result[$keys[0]])) {
                $result[$keys[0]] = [];
            }

            $result[$keys[0]][$entity[$keys[0]]->getId()] = $entity[$keys[0]];
        }

        return $result;
    }

    private function hydrateElementData($element, $externalEntities)
    {
        $data = $element->getData();

        foreach ($this->elementConfig[$element->getType()]['fields'] as $dataFieldName => $dataFieldOptions) {
            // Field is an entity
            if (isset($data[$dataFieldName]) && isset($dataFieldOptions['class'])) {
                $entityClass = str_replace('\\', '', (string) $dataFieldOptions['class']);
                if (isset($dataFieldOptions['multiple']) && $dataFieldOptions['multiple']) {
                    $ids = $data[$dataFieldName];
                    $data[$dataFieldName] = [];
                    foreach ($ids as $id) {
                        if (isset($externalEntities[$entityClass][$id])) {
                            $data[$dataFieldName][$id] = $externalEntities[$entityClass][$id];
                        }
                    }
                } elseif (isset($externalEntities[$entityClass])) {
                    $dataOffset = $data[$dataFieldName];
                    if (is_iterable($dataOffset)) {
                        $dataOffset = $dataOffset[0] ?? null;
                    }

                    if (!is_object($dataOffset) && isset($externalEntities[$entityClass][$dataOffset])) {
                        $data[$dataFieldName] = $externalEntities[$entityClass][$dataOffset];
                    } else {
                        unset($data[$dataFieldName]);
                    }
                } else {
                    unset($data[$dataFieldName]);
                }
            }
        }

        $element->setData($data);
    }

    public function findChartByDates(\DateTime $start, \DateTime $end)
    {
        return $this->elementRepository->findChartByDates($start, $end, 'e.type', 'e.type');
    }

    public function setLocale(Element $entity, string $locale): void
    {
        $entity->setLocale($locale);
        $this->em->refresh($entity);
    }

    public function findTranslations(Element $entity)
    {
        return $this->translationRepository->findTranslations($entity);
    }

    public function findTranslation(Element $entity, $locale)
    {
        $translations = $this->findTranslations($entity);

        return $translations[$locale] ?? [];
    }

    public function setTranslation(Element $entity, $locale, $data)
    {
        foreach ($data as $key => $value) {
            $this->translationRepository->translate($entity, $key, $locale, $value);
        }

        $this->em->persist($entity);
        $this->em->flush();
    }

    public function addTranslationFormFields($typeConfig)
    {
        $field = [];
        $translatableFields = [];
        $translatableFields = array_filter($typeConfig['fields'], fn ($field) => $field['translatable']);

        return $this->arraytoFormService->createForm($translatableFields, 'data', []);
    }

    public function addView(Element $entity): void
    {
        if (!$this->elementConfig[$entity->getType()]['enable_view_count']) {
            return;
        }

        $sessionName = 'element_view_'.$entity->getId();
        $sessionService = $this->requestStack->getSession();
        $session = $sessionService->get($sessionName, false);
        if (!$session) {
            $this->em->detach($entity);
            // Problème, on modifie la db alors que les data de l'entité on été chargé (ça efface les images)
            $viewedEntity = $this->elementRepository->findOneById($entity->getId());
            $viewedEntity->addView();

            $this->em->persist($viewedEntity);
            $this->em->flush();
            $sessionService->set($sessionName, true);
        }
    }

    public function getBreadcrumb(Element $entity): array
    {
        if ($entity->getIsCategories()) {
            $categories = $entity->getCategories();
        }

        return [
            'content.element.type.'.$entity->getType().'_menu' => '',
            ucfirst($entity->getName()) => ['route' => 'tigris_content_element_show', 'params' => ['type' => $entity->getType(), 'slug' => $entity->getSlug()]],
        ];
    }
}
