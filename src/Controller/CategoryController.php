<?php

namespace Tigris\ContentBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Tigris\BaseBundle\Controller\BaseController;
use Tigris\ContentBundle\Entity\Category;
use Tigris\ContentBundle\Manager\ElementManager;
use Tigris\ContentBundle\Repository\CategoryRepository;
use Twig\Environment;

#[Route('/category')]
class CategoryController extends BaseController
{
    #[Route('/show/{id<\d+>}/{page<\d+>}', name: 'tigris_content_category_show_id', defaults: ['page' => 1], options: ['expose' => true])]
    #[Route('/{slug}/{page<\d+>}', requirements: ['slug' => '[a-zA-Z1-9\-_]+'], defaults: ['page' => 1], options: ['expose' => true])]
    public function show(Environment $templating, Category $entity, int $page, ElementManager $elementManager): Response
    {
        $type = $entity->getTypes()[0] ?? false;
        $breadcrumb = [];

        if ($type) {
            $breadcrumb['content.element.type.'.$type.'_menu'] = ['route' => 'tigris_content_element_list', 'params' => ['type' => $type]];
        }

        $breadcrumb = $this->getParentBreadcrumb($entity, $breadcrumb);

        $this->generateBreadcrumbs($breadcrumb);

        $count = 15;
        $data = $elementManager->findByCategory($entity, $count, ($page - 1) * $count);
        $nbPages = ceil($data->count() / $count);

        $templates = [];
        foreach ($entity->getTypes() as $type) {
            $templatePath = '@TigrisContent/element/template/'.$type.'_preview.html.twig';
            if (!$templating->getLoader()->exists($templatePath)) {
                $templatePath = '@TigrisContent/element/preview.html.twig';
            }

            $templates[$type] = $templatePath;
        }

        return $this->render('@TigrisContent/category/show.html.twig', [
            'entity' => $entity,
            'elements' => $data->getIterator(),
            'nbPages' => $nbPages,
            'page' => $page,
            'templates' => $templates,
        ]);
    }

    public function _list(string $type, CategoryRepository $categoryRepository, array $criteria = []): Response
    {
        $criteria = array_merge($criteria, ['count' => 20]);
        $criteria['public'] = true;
        $criteria['type'] = $type;
        
        $entities = $categoryRepository->findData($criteria);

        return $this->render('@TigrisContent/category/_list.html.twig', [
            'entities' => $entities,
            'type' => $type,
        ]);
    }

    #[Route('/list/{type}/{page<\d+>}', defaults: ['page' => 1], requirements: ['type' => '[a-zA-Z1-9\-_]+'])]
    public function list(string $type, CategoryRepository $categoryRepository): Response
    {
        $entities = $categoryRepository->findData(['type' => $type, 'public' => true]);

        return $this->render('@TigrisContent/category/list.html.twig', [
            'entities' => $entities,
            'type' => $type,
        ]);
    }

    private function getParentBreadcrumb(Category $category, array $breadcrumb = []): array 
    {
        $parent = $category->getParent();
        if ($parent !== null) {
            
            $breadcrumb[$parent->getName()] = ['route' => 'tigris_content_category_show', 'params' => ['slug' => $parent->getSlug()]];
            $breadcrumb = $this->getParentBreadcrumb($category->getParent(), $breadcrumb);
        }

        return $breadcrumb;
    }
}