<?php

namespace Tigris\ContentBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Controller\BaseController;
use Tigris\BaseBundle\Form\ConfiguredForm\Type\ConfiguredFormType;
use Tigris\BaseBundle\Service\ArrayToFormService;
use Tigris\BaseBundle\Traits\FormTrait;
use Tigris\ContentBundle\Entity\Element;
use Tigris\ContentBundle\Event\ElementEvent;
use Tigris\ContentBundle\Event\Events;
use Tigris\ContentBundle\Form\Type\CreateElementType;
use Tigris\ContentBundle\Form\Type\ElementType;
use Tigris\ContentBundle\Manager\ElementManager;
use Tigris\ContentBundle\Repository\CategoryRepository;
use Tigris\ContentBundle\Repository\ElementRepository;
use Twig\Environment;

#[Route('')]
class ElementController extends BaseController
{
    use FormTrait;

    #[Route('/content/user/list', options: ['expose' => true], requirements: ['type' => '[a-zA-Z0-9\-_]+'])]
    #[IsGranted('ROLE_USER')]
    public function userList(): Response
    {
        $elementTypes = $this->getParameter('tigris_content.elements');

        $elementTypes = array_filter($elementTypes, fn ($value) => $value['allow_user_adding']);

        return $this->render('@TigrisContent/element/user_list.html.twig', ['types' => $elementTypes]);
    }

    #[Route('/content/data', options: ['expose' => true])]
    #[IsGranted('ROLE_USER')]
    public function userData(Request $request, ElementRepository $repository): JsonResponse
    {
        $user = $this->getUser();
        // Prepare query
        $criteria = $this->getCriteria($request);
        $filters = [
            'search' => $request->query->get('search', ''),
            'type' => $request->query->get('type', ''),
            'public' => $request->query->get('public', ''),
            'user' => $user,
        ];

        // Get data
        $data = $repository->findData($criteria, $filters);

        return $this->paginatorToJsonResponse($data);
    }

    #[Route('/content/{type}/new', options: ['expose' => true], requirements: ['type' => '[a-zA-Z0-9\-_]+'], methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_USER')]
    public function new(Request $request, $type, TranslatorInterface $translator, EventDispatcherInterface $eventDispatcher, EntityManagerInterface $em): Response
    {
        $entity = (new Element())
            ->setType($type)
            ->setUser($this->getUser());

        $this->denyAccessUnlessGranted('edit', $entity);

        $form = $this->createForm(CreateElementType::class, $entity, ['attr' => ['class' => 'no-ajax']]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $event = new GenericEvent($entity, ['backEnd' => false]);
            $eventDispatcher->dispatch($event, Events::CONTENT_ELEMENT_ADDED);

            if (!$request->isXmlHttpRequest()) {
                $this->addFlash(
                    'success',
                    $translator->trans('content.element.add.success')
                );

                return $this->redirectToRoute('tigris_content_element_edit', ['id' => $entity->getId()]);
            }
        }

        return $this->render('@TigrisContent/element/new.html.twig', ['entity' => $entity, 'form' => $form]);
    }

    #[Route('/content/{id<\d+>}/edit', options: ['expose' => true], methods: ['GET', 'PUT'])]
    #[IsGranted('ROLE_USER')]
    public function edit(
        Request $request,
        #[MapEntity(expr: 'repository.findOneById(id)')]
        Element $entity,
        EventDispatcherInterface $eventDispatcher,
        ElementManager $elementManager,
        EntityManagerInterface $em
    ): Response {
        $this->denyAccessUnlessGranted('edit', $entity);
        $oldEntity = clone $entity;

        $elementTypes = $this->getParameter('tigris_content.elements');
        $type = $entity->getType();

        $form = $this->createForm(ElementType::class, $entity, ['attr' => ['class' => 'no-ajax'], 'method' => 'PUT']);

        $form->add('data', ConfiguredFormType::class, [
            'fields' => $elementTypes[$entity->getType()]['fields'],
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();
            $em->clear();

            $elementManager->includeExternalEntities($entity);

            $event = new GenericEvent($entity, ['backEnd' => true, 'oldEntity' => $oldEntity]);
            $eventDispatcher->dispatch($event, Events::CONTENT_ELEMENT_UPDATED);

            $this->addFlash('success', 'content.element.edit.success');

            return $this->redirectToRoute('tigris_content_element_list', ['type' => $entity->getType()]);
        }

        return $this->render('@TigrisContent/element/edit.html.twig', ['entity' => $entity, 'form' => $form]);
    }

    #[Route('/{id<\d+>}/remove', options: ['expose' => true], methods: ['GET', 'DELETE'])]
    #[IsGranted('ROLE_USER')]
    public function remove(Request $request, Element $entity, EventDispatcherInterface $eventDispatcher, EntityManagerInterface $em): Response
    {
        $form = $this->getDeleteForm($entity, 'tigris_content_element_');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($entity);
            $em->flush();

            $event = new GenericEvent($entity, ['backEnd' => false]);
            $eventDispatcher->dispatch($event, Events::CONTENT_ELEMENT_DELETED);

            return $this->redirectToRoute('tigris_content_element_userlist');
        }

        return $this->render('@TigrisContent/element/remove.html.twig', ['form' => $form->createView(), 'entity' => $entity]);
    }

    #[Route('/{type}/list', requirements: ['type' => '[a-zA-Z1-9\-_]+'])]
    public function list(Request $request, string $type, RequestStack $requestStack, ElementManager $elementManager, CategoryRepository $categoryRepository, Environment $templating, $categories = [], $count = null, $sort = null, $desc = true): Response
    {
        $elementTypes = $this->getParameter('tigris_content.elements');
        $templateNamePrefix = '';

        if (null !== $requestStack->getParentRequest()) {
            $templateNamePrefix = '_';
        } else {
            $this->generateBreadcrumbs([
                'content.element.type.'.$type.'_menu' => ['route' => 'tigris_content_element_list', 'params' => ['type' => $type]],
            ]);
        }

        if (!isset($elementTypes[$type])) {
            throw $this->createNotFoundException();
        }

        $elementInfos = $elementTypes[$type];
        $count ??= $elementInfos['count_per_page'];
        $sort ??= $elementInfos['sort_by'];
        $desc = null != $sort ? $elementInfos['order_desc'] : $desc;
        $categories = empty($categories) ? $request->query->all('categories') : $categories;
        $search = $request->query->get('search', null);

        $page = $request->query->get('p', 1);
        $offset = ($page - 1) * $count;

        $criteria = [
            'count' => $count,
            'begin' => $offset,
            'order' => $sort,
            'rev' => $desc,
            'categories' => $categories,
            'search' => $search,
        ];

        $entities = $elementManager->findByType($type, $criteria);
        $categoryEntities = $categoryRepository->getMenuTree($type);
        $pages = ceil($entities->count() / $count);
        $templatePath = '@TigrisContent/element/template/'.$type.'_list.html.twig';
        $renderData = [
            'entities' => $entities,
            'count' => $entities->count(),
            'categories' => $categoryEntities,
            'selectedCategories' => $categories,
            'embeded' => null !== $requestStack->getParentRequest(),
            'pages' => $pages,
            'page' => $page,
            'socialTools' => [],
            'type' => $type,
        ];

        if ($templating->getLoader()->exists($templatePath)) {
            return $this->render($templatePath, $renderData);
        }

        return $this->render('@TigrisContent/element/'.$templateNamePrefix.'list.html.twig', $renderData);
    }

    public function _listAction(Request $request, string $type, RequestStack $requestStack, ElementManager $elementManager, CategoryRepository $categoryRepository, Environment $templating, array $categories = [], int $count = 10, string $sort = 'publishedAt', bool $desc = true): Response
    {
        return $this->list($request, $type, $requestStack, $elementManager, $categoryRepository, $templating, $categories, $count, $sort, $desc);
    }

    #[Route('/element/{type}/{id<\d+>}', name: 'tigris_content_element_show_id', requirements: ['type' => '[a-zA-Z1-9\-_]+'], defaults: ['type' => 'article'], options: ['expose' => true])]
    #[Route('/{type}/{slug}', requirements: ['type' => '[a-zA-Z1-9\-_]+', 'slug' => '[a-zA-Z0-9\-_]+'], options: ['expose' => true])]
    public function show(
        Request $request,
        ?int $id,
        ?string $slug,
        ElementManager $elementManager,
        Environment $templating,
        EventDispatcherInterface $eventDispatcher
    ): Response {
        if (null !== $id) {
            $entity = $elementManager->findOneById($id);
        } else {
            $entity = $elementManager->findOneBySlug($slug);
        }

        if (null === $entity) {
            throw $this->createNotFoundException();
        }

        $firstCategory = $entity->getCategories()->first();
        $breadcrumb = [
            'content.element.type.'.$entity->getType().'_menu' => ['route' => 'tigris_content_element_list', 'params' => ['type' => $entity->getType()]],
        ];

        if ($firstCategory) {
            $breadcrumb[ucfirst((string) $firstCategory->getName())] = ['route' => 'tigris_content_category_show', 'params' => ['slug' => $firstCategory->getSlug()]];
        }

        $breadcrumb[ucfirst($entity->getName())] = ['route' => 'tigris_content_element_show', 'params' => ['type' => $entity->getType(), 'slug' => $entity->getSlug()]];

        $this->generateBreadcrumbs($breadcrumb);

        $this->denyAccessUnlessGranted('view', $entity);
        $canEdit = $this->isGranted('ROLE_SUPER_ADMIN');
        $templatePath = '@TigrisContent/element/template/'.$entity->getType().'_show.html.twig';

        $isFragment = strpos((string) $request->server->get('REQUEST_URI'), '_fragment');

        if (!$templating->getLoader()->exists($templatePath)) {
            $templatePath = '@TigrisContent/element/show.html.twig';
        }

        $elementManager->addView($entity);

        $event = new ElementEvent($entity);
        $eventDispatcher->dispatch($event, Events::SHOW_ELEMENT);

        return $this->render($templatePath, \array_merge([
            'entity' => $entity,
            'edit' => $canEdit,
            'isFragment' => $isFragment,
        ], $event->getViewParams()));
    }
}
