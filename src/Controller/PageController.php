<?php

namespace Tigris\ContentBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Controller\BaseController;
use Tigris\ContentBundle\Entity\Page;
use Tigris\ContentBundle\Event\Events;
use Tigris\ContentBundle\Event\PageEvent;
use Tigris\ContentBundle\Repository\PageRepository;

#[Route('')]
class PageController extends BaseController
{
    #[Route('/')]
    public function index(TranslatorInterface $translator, PageRepository $pageRepository, EventDispatcherInterface $eventDispatcher): Response
    {
        $page = $pageRepository->findHomePage();
        if (!$page instanceof \Tigris\ContentBundle\Entity\Page) {
            throw $this->createNotFoundException($translator->trans('page.not_found'));
        }

        return $this->show($page, $eventDispatcher);
    }

    #[Route('/page/{id<\d+>}', name: 'tigris_content_page_show_id')]
    #[Route('/{slug}', requirements: ['slug' => '[a-zA-Z0-9\-_]+'], options: ['expose' => true])]
    public function show(Page $page, EventDispatcherInterface $eventDispatcher): Response
    {
        $this->generateBreadcrumbs([
            $page->getName() => ['route' => 'tigris_content_page_show', 'params' => ['slug' => $page->getSlug()]],
        ]);

        $this->denyAccessUnlessGranted('view', $page);
        $admin = ($this->isGranted('ROLE_CONTENT_PAGE_ADMIN') || $this->isGranted('ROLE_ADMIN'));

        $event = new PageEvent($page);
        $eventDispatcher->dispatch($event, Events::SHOW_PAGE);

        switch ($page->getType()) {
            case Page::TYPE_TEXT:
                return $this->render('@TigrisContent/page/template/'.$page->getTemplate().'.html.twig', \array_merge(['entity' => $page, 'edit' => $admin], $event->getViewParams()));
            case Page::TYPE_URL:
                return $this->redirect($page->getUrl());
            case Page::TYPE_CONTENT_LIST:
                return $this->forward(ElementController::class.'::list', array_merge(['type' => $page->getContentType()], $event->getViewParams()));
        }

        throw $this->createNotFoundException('Page not found');
    }
}
