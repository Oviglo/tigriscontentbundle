<?php

namespace Tigris\ContentBundle\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Controller\Admin\BaseController;
use Tigris\BaseBundle\Form\ConfiguredForm\Type\ConfiguredFormType;
use Tigris\BaseBundle\Form\Type\FormActionsType;
use Tigris\BaseBundle\Traits\FormTrait;
use Tigris\ContentBundle\Entity\Page;
use Tigris\ContentBundle\Form\Type\PageContentOptionsType;
use Tigris\ContentBundle\Form\Type\PageType;
use Tigris\ContentBundle\Repository\PageRepository;

#[Route('/admin/content/page')]
#[IsGranted('ROLE_ADMIN')]
class PageController extends BaseController
{
    use FormTrait;

    public function generateBreadcrumbs(array $routes = []): void
    {
        $routes = array_merge([
            'menu.content.content' => null,
            'menu.content.pages' => ['route' => 'tigris_content_admin_page_index'],
        ], $routes);

        parent::generateBreadcrumbs($routes);
    }

    #[Route('/')]
    public function index(): Response
    {
        $this->generateBreadcrumbs();

        $pageTemplates = $this->getParameter('tigris_content.page_templates');
        $menus = $this->getParameter('tigris_content.menus');

        return $this->render('@TigrisContent\admin\page\index.html.twig', [
            'templates' => $pageTemplates,
            'menus' => $menus,
        ]);
    }

    #[Route('/data', options: ['expose' => 'admin'])]
    public function list(Request $request, PageRepository $pageRepository): JsonResponse
    {
        // Prepare query
        $criteria = $this->getCriteria($request);

        $filters = [
            'search' => $request->query->get('search', ''),
            'public' => $request->query->get('public', 1),
            'template' => $request->query->get('template', ''),
            'menu' => $request->query->get('menu', ''),
        ];

        // Get data
        $data = $pageRepository->findData($criteria, $filters);

        return $this->paginatorToJsonResponse($data);
    }

    #[Route('/new', options: ['expose' => 'admin'], methods: ['GET', 'POST'])]
    public function new(Request $request, TranslatorInterface $translator, EntityManagerInterface $em): Response
    {
        $this->generateBreadcrumbs();
        $entity = new Page();

        $form = $this->createForm(PageType::class, $entity, [
            'action' => $this->generateUrl('tigris_content_admin_page_new'),
            'method' => 'POST',
        ]);
        $this->addFormActions($form, $this->generateUrl('tigris_content_admin_page_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $entity->setUser($user);
            $em->persist($entity);
            $em->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('page.add.success'), 'entity' => $entity]);
            }
            $this->addFlash(
                'success',
                $translator->trans('page.add.success')
            );

            return $this->redirectToRoute('tigris_content_admin_page_index');
        }

        return $this->render('@TigrisContent\admin\page\new.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/{id<\d+>}/edit', options: ['expose' => 'admin'], methods: ['GET', 'PUT'])]
    public function edit(Request $request, Page $entity, TranslatorInterface $translator, EntityManagerInterface $em): Response
    {
        $this->generateBreadcrumbs();
        $form = $this->createForm(PageType::class, $entity, [
            'action' => $this->generateUrl('tigris_content_admin_page_edit', ['id' => $entity->getId()]),
            'method' => 'PUT',
        ]);
        $this->addFormActions($form, $this->generateUrl('tigris_content_admin_page_index'));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('page.edit.success'), 'entity' => $entity]);
            }
            $this->addFlash(
                'success',
                $translator->trans('page.edit.success')
            );

            return $this->redirectToRoute('tigris_content_admin_page_index');
        }

        return $this->render('@TigrisContent\admin\page\edit.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/{id<\d+>}/save', options: ['expose' => 'admin'], methods: ['GET', 'PUT', 'POST'])]
    public function save(Request $request, Page $entity, TranslatorInterface $translator, EntityManagerInterface $em): JsonResponse
    {
        $data = $request->request->all('data');
        if (!empty($data)) {
            $entity->setData($data);
            $em->persist($entity);
            $em->flush();

            return new JsonResponse(['status' => 'success', 'message' => $translator->trans('page.save.success')]);
        }

        return new JsonResponse(['status' => 'error', 'message' => $translator->trans('page.save.empty_data')]);
    }

    #[Route('/{id<\d+>}/remove', options: ['expose' => 'admin'], methods: ['GET', 'DELETE'])]
    public function remove(Request $request, Page $entity, TranslatorInterface $translator, EntityManagerInterface $em): Response
    {
        $form = $this->getDeleteForm($entity, 'tigris_content_admin_page_');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($entity);
            $em->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('page.delete.success')]);
            }
            $this->addFlash(
                'success',
                'page.delete.success'
            );

            return $this->redirectToRoute('tigris_content_admin_page_index');
        }

        return $this->render('@TigrisContent\admin\page\remove.html.twig', [
            'form' => $form->createView(),
            'entity' => $entity,
        ]);
    }

    #[Route('/sort', options: ['expose' => 'admin'], methods: ['GET', 'POST'])]
    public function sort(Request $request, TranslatorInterface $translator, PageRepository $pageRepository, EntityManagerInterface $em): Response
    {
        $form = $this->createFormBuilder(null, [
            'attr' => [
                'novalidate' => 'novalidate',
            ],
        ])
            ->setMethod(Request::METHOD_POST)
            ->setAction($this->generateUrl('tigris_content_admin_page_sort'))
            ->add('actions', FormActionsType::class, [
                'label' => false,
                'buttons' => [
                    'save' => [
                        'type' => SubmitType::class,
                        'options' => ['label' => 'button.save'],
                    ],
                    'cancel' => [
                        'type' => ButtonType::class,
                        'options' => [
                            'as_link' => true,
                            'label' => 'button.cancel',
                            'attr' => ['data-cancel' => true, 'href' => $this->generateUrl('tigris_content_admin_page_index')],
                        ],
                    ],
                ],
            ])
            ->getForm()
        ;

        $form->handleRequest($request);

        $entities = $pageRepository->getTree();

        if ($form->isSubmitted() && $form->isValid()) {
            $allEntities = $pageRepository->findAll();
            $positions = $request->request->all('position');
            // asort($positions);
            foreach ($positions as $id => $position) {
                foreach ($allEntities as $entity) {
                    if ($entity->getId() === $id) {
                        $entity->setPosition($position);
                        $em->persist($entity);

                        break;
                    }
                }
            }

            $em->flush();

            $pageRepository->reorderAll('position');

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('page.sort.success')]);
            }
            $this->addFlash('success', 'page.sort.success');

            return $this->redirectToRoute('tigris_content_admin_page_index');
        }

        return $this->render('@TigrisContent\admin\page\sort.html.twig', [
            'form' => $form,
            'entities' => $entities,
        ]);
    }

    #[Route('/{id<\d+>}/content-options', options: ['expose' => 'admin'], methods: ['GET', 'PUT'])]
    public function contentOptions(
        Request $request,
        Page $entity,
        EntityManagerInterface $em,
        #[Autowire('%tigris_content.page_templates%')]
        array $pageTemplates,
        TranslatorInterface $translator
    ): Response {
        $form = $this->createForm(PageContentOptionsType::class, $entity, [
            'action' => $this->generateUrl('tigris_content_admin_page_contentoptions', ['id' => $entity->getId()]),
            'method' => 'PUT',
        ]);

        $form->add('contentOptions', ConfiguredFormType::class, [
            'fields' => $pageTemplates[$entity->getTemplate()]['content_options']['fields'],
        ]);

        $this->addFormActions($form, $this->generateUrl('tigris_content_admin_page_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            return new JsonResponse(['status' => 'success', 'message' => $translator->trans('page.content_options.success')]);
        }

        return $this->render('@TigrisContent\admin\page\content_options.html.twig', [
            'form' => $form,
            'entity' => $entity,
        ]);
    }
}
