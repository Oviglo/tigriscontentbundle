<?php

namespace Tigris\ContentBundle\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Form\ConfiguredForm\Type\ConfiguredFormType;
use Tigris\BaseBundle\Controller\Admin\BaseController;
use Tigris\BaseBundle\Controller\ChartControllerTrait;
use Tigris\BaseBundle\Form\Type\FormActionsType;
use Tigris\BaseBundle\Service\ArrayToFormService;
use Tigris\BaseBundle\Traits\FormTrait;
use Tigris\BaseBundle\Utils\Utils;
use Tigris\ContentBundle\Entity\Element;
use Tigris\ContentBundle\Event\Events;
use Tigris\ContentBundle\Form\Type\CreateElementType;
use Tigris\ContentBundle\Form\Type\ElementTranslationType;
use Tigris\ContentBundle\Form\Type\ElementType;
use Tigris\ContentBundle\Manager\ElementManager;
use Tigris\ContentBundle\Repository\ElementRepository;

#[Route('/admin/content/element')]
#[IsGranted('ROLE_ADMIN')]
class ElementController extends BaseController
{
    use FormTrait;
    use ChartControllerTrait;

    #[Route('/{type}', requirements: ['type' => '[a-zA-Z0-9\-_]+'])]
    public function index(Request $request, string $type): Response
    {
        $this->generateBreadcrumbs([
            'menu.content.elements' => ['route' => 'tigris_content_admin_element_index', 'params' => ['type' => $type]],
        ]);

        $elementTypes = $this->getParameter('tigris_content.elements');
        $allowTranslate = $this->getParameter('tigris_content.allow_translate');
        $locales = explode('|', $this->getParameter('app_locales'));
        $currentLocale = $this->getParameter('locale');
        $criteria = $this->getCriteria($request, 'adminContentElementCriteria');

        return $this->render('@TigrisContent/admin/element/index.html.twig', [
            'types' => $elementTypes,
            'type' => $elementTypes[$type],
            'typeName' => $type,
            'allowTranslate' => $allowTranslate,
            'locales' => $locales,
            'currentLocale' => $currentLocale,
            'criteria' => $criteria,
        ]);
    }

    #[Route('/{type}/data', requirements: ['type' => '[a-zA-Z0-9\-_]+'], options: ['expose' => 'admin'])]
    public function data(string $type, Request $request, ElementRepository $elementRepository): JsonResponse
    {
        $criteria = $this->getCriteria($request, 'adminContentElementCriteria');
        $filters = [
            'search' => $request->query->get('search', ''),
            'public' => $request->query->get('public', 1),
            'type' => $type,
        ];

        // Get data
        $data = $elementRepository->findData($criteria, $filters);

        return $this->paginatorToJsonResponse($data);
    }

    #[Route('/{type}/new', options: ['expose' => 'admin'], requirements: ['type' => '[a-zA-Z0-9\-_]+'])]
    public function new(Request $request, string $type, EventDispatcherInterface $eventDispatcher, EntityManagerInterface $em): Response
    {
        $this->generateBreadcrumbs([
            'menu.content.elements' => ['route' => 'tigris_content_admin_element_index', 'params' => ['type' => $type]],
            'content.element.add.add' => ['route' => 'tigris_content_admin_element_new', 'params' => ['type' => $type]],
        ]);

        $user = $this->getUser();

        $entity = (new Element())
            ->setUser($user)
            ->setPublic(false)
        ;

        if (null !== $type) {
            $entity->setType($type);
        }
        $form = $this->createForm(CreateElementType::class, $entity, [
            'action' => $this->generateUrl('tigris_content_admin_element_new', ['type' => $entity->getType()]),
            'attr' => ['no-ajax' => true],
        ]);
        $this->addFormActions(
            $form,
            $this->generateUrl('tigris_content_admin_element_index', ['type' => $entity->getType()]),
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $event = new GenericEvent($entity, ['backEnd' => true]);
            $eventDispatcher->dispatch($event, Events::CONTENT_ELEMENT_ADDED);

            $this->addFlash('success', 'content.element.add.success');

            return $this->redirectToRoute('tigris_content_admin_element_edit', ['id' => $entity->getId()]);
        }

        return $this->render('@TigrisContent/admin/element/new.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/{id}/edit', requirements: ['id' => '\d+'], options: ['expose' => 'admin'], methods: ['GET', 'PUT'])]
    public function edit(
        Request $request,
        int $id,
        EventDispatcherInterface $eventDispatcher,
        ElementManager $elementManager,
        TranslatorInterface $translator,
        EntityManagerInterface $em
    ): Response {
        $entity = $elementManager->findOneById($id);
        $this->generateBreadcrumbs([
            'menu.content.elements' => ['route' => 'tigris_content_admin_element_index', 'params' => ['type' => $entity->getType()]],
            'content.element.edit.edit' => ['route' => 'tigris_content_admin_element_edit', 'params' => ['id' => $entity->getId()]],
        ]);

        $oldEntity = clone $entity;

        $elementTypes = $this->getParameter('tigris_content.elements');
        $form = $this->createForm(ElementType::class, $entity, [
            'method' => Request::METHOD_PUT,
        ]);
        $this->addFormActions(
            $form,
            $this->generateUrl('tigris_content_admin_element_index', ['type' => $entity->getType()]),
        );
        $form->add('data', ConfiguredFormType::class, [
            'fields' => $elementTypes[$entity->getType()]['fields'],
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();
            $em->clear();

            $elementManager->includeExternalEntities($entity);

            $event = new GenericEvent($entity, ['backEnd' => true, 'oldEntity' => $oldEntity]);
            $eventDispatcher->dispatch($event, Events::CONTENT_ELEMENT_UPDATED);

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('content.element.edit.success')]);
            } else {
                $this->addFlash(
                    'success',
                    'content.element.edit.success'
                );

                return $this->redirectToRoute('tigris_content_admin_element_index', ['type' => $entity->getType()]);
            }
        }

        return $this->render('@TigrisContent/admin/element/edit.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/{id}/remove', requirements: ['id' => '\d+'], options: ['expose' => 'admin'], methods: ['GET', 'DELETE'])]
    public function remove(Request $request, Element $entity, EventDispatcherInterface $eventDispatcher, TranslatorInterface $translator, EntityManagerInterface $em): Response
    {
        $form = $this->getConfirmationForm(
            $this->generateUrl('tigris_content_admin_element_remove', ['id' => $entity->getId()]),
            $this->generateUrl('tigris_content_admin_element_index', ['type' => $entity->getType()])
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($entity);
            $em->flush();

            $event = new GenericEvent($entity, ['backEnd' => true]);
            $eventDispatcher->dispatch($event, Events::CONTENT_ELEMENT_DELETED);

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('content.element.delete.success')]);
            } else {
                $this->addFlash(
                    'success',
                    'content.element.delete.success'
                );

                return $this->redirectToRoute('tigris_content_admin_element_index', ['type' => $entity->getType()]);
            }
        }

        return $this->render('@TigrisContent/admin/element/remove.html.twig', [
            'form' => $form->createView(),
            'entity' => $entity,
        ]);
    }

    #[Route('/{id}/save', requirements: ['id' => '\d+'], options: ['expose' => 'admin'], methods: ['POST'])]
    public function save(Request $request, Element $entity, TranslatorInterface $translator, EntityManagerInterface $em): JsonResponse
    {
        $data = $request->request->all('data');
        if ([] === $data) {
            $entity->setData($data);
            $em->persist($entity);
            $em->flush();
        }

        return new JsonResponse(['message' => $translator->trans('content.element.save.success')]);
    }

    #[Route('/{id}/update-part', requirements: ['id' => '\d+'], options: ['expose' => 'admin'], methods: ['PUT'])]
    public function updatePart(Request $request, Element $entity, EventDispatcherInterface $eventDispatcher, ElementManager $elementManager, TranslatorInterface $translator, EntityManagerInterface $em): JsonResponse
    {
        $this->denyAccessUnlessGranted('edit', $entity);
        $data = $request->request->get('data', null);
        $value = $request->request->get('value', null);
        $oldEntity = clone $entity;

        if (!is_null($data) && !is_null($value)) {
            $method = 'set'.ucfirst($data);

            if (method_exists($entity, $method)) {
                $entity->$method(Utils::formatValue($value));
                $em->persist($entity);
                $em->flush();
                // $em->clear($entity);

                $elementManager->includeExternalEntities($entity);

                $event = new GenericEvent($entity, ['backEnd' => true, 'oldEntity' => $oldEntity]);
                $eventDispatcher->dispatch($event, Events::CONTENT_ELEMENT_UPDATED);
            }
        }

        return new JsonResponse(['status' => 'success', 'message' => $translator->trans('content.element.edit.success')]);
    }

    #[Route('/chart/chart', options: ['expose' => 'admin'])]
    public function chart(Request $request, elementManager $elementManager, TranslatorInterface $translator): Response
    {
        $criteria = $this->getDateCriteria($request);

        $board = $this->getStatsBoard(
            $criteria['startDate'],
            $criteria['endDate'],
            false
        );
        $labels = $board['labels'];
        $stats = $board['stats'];
        $dateStart = $board['dates']['start'];
        $dateEnd = $board['dates']['end'];

        $entities = $elementManager->findChartByDates($dateStart, $dateEnd);
        $formattedStats = [];

        foreach ($entities as $entity) {
            if (!isset($formattedStats[$entity['type']])) {
                $formattedStats[$entity['type']] = [
                    'label' => $translator->trans('content.element.type.'.$entity['type']),
                    'data' => $stats,
                ];
            }
        }

        foreach ($entities as $entity) {
            $formattedStats[$entity['type']]['data'][(int) $entity['year'].'-'.(int) $entity['month']] = (int) $entity['nb'];
        }

        $finalStats = [];
        foreach (array_keys($formattedStats) as $name) {
            $finalStats[] = [
                'data' => array_values($formattedStats[$name]['data']),
                'label' => $formattedStats[$name]['label'],
            ];
        }

        return new Jsonresponse(['labels' => $labels, 'stats' => $finalStats]);
    }

    #[Route('/{id}/translate/{locale}', requirements: ['id' => '\d+', 'locale' => '[a-z]{2}'], options: ['expose' => 'admin'])]
    public function translate(
        Request $request,
        int $id,
        string $locale,
        ElementManager $elementManager,
        TranslatorInterface $translator
    ): Response {
        $entity = $elementManager->findOneById($id);
        $this->generateBreadcrumbs([
            'menu.content.elements' => ['route' => 'tigris_content_admin_element_index', 'params' => ['type' => $entity->getType()]],
            $translator->trans('content.element.translate.title', ['%lang%' => $translator->trans('locale.'.$locale)]) => ['route' => 'tigris_content_admin_element_translate', 'params' => ['id' => $entity->getId(), 'locale' => $locale]],
        ]);

        // $elementManager->setLocale($entity, $locale);
        $translation = $elementManager->findTranslation($entity, $locale);
        $elementTypes = $this->getParameter('tigris_content.elements');
        $type = $elementTypes[$entity->getType()];

        $form = $this->createForm(ElementTranslationType::class)
        ->add('actions', FormActionsType::class, [
            'label' => false,
            'buttons' => [
                'save' => [
                    'type' => SubmitType::class,
                    'options' => ['label' => 'button.save'],
                ],
                'cancel' => [
                    'type' => ButtonType::class,
                    'options' => [
                        'as_link' => true,
                        'label' => 'button.cancel',
                        'attr' => ['data-cancel' => true, 'href' => $this->generateUrl('tigris_content_admin_element_index', ['type' => $entity->getType()])],
                    ],
                ],
            ],
        ]);

        $dataForm = $elementManager->addTranslationFormFields($type);

        $form->add($dataForm);

        $form->setData($translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $elementManager->setTranslation($entity, $locale, $data);

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('content.element.translate.success')]);
            }

            $this->addFlash(
                'success',
                $translator->trans('content.element.translate.success')
            );

            return $this->redirectToRoute('tigris_content_admin_element_index', ['type' => $entity->getType()]);
        }

        return $this->render('@TigrisContent/admin/element/translate.html.twig', [
            'form' => $form,
            'entity' => $entity,
            'locale' => $locale,
        ]);
    }

    #[Route('/{type}/sort', requirements: ['type' => '[a-zA-Z0-9\-_]+'], options: ['expose' => 'admin'], methods: ['GET', 'POST'])]
    public function sort(Request $request, TranslatorInterface $translator, ElementRepository $elementRepository, string $type, EntityManagerInterface $em): Response
    {
        $form = $this->createFormBuilder(null, [
                'attr' => [
                    'novalidate' => 'novalidate',
                ],
            ])
            ->setMethod(\Symfony\Component\HttpFoundation\Request::METHOD_POST)
            ->setAction($this->generateUrl('tigris_content_admin_element_sort'))
            ->add('actions', FormActionsType::class, [
                'label' => false,
                'buttons' => [
                    'save' => [
                        'type' => SubmitType::class,
                        'options' => ['label' => 'button.save'],
                    ],
                    'cancel' => [
                        'type' => ButtonType::class,
                        'options' => [
                            'as_link' => true,
                            'label' => 'button.cancel',
                            'attr' => ['data-cancel' => true, 'href' => $this->generateUrl('tigris_content_admin_page_index')],
                        ],
                    ],
                ],
            ])
            ->getForm()
        ;

        $form->handleRequest($request);

        $filters = [
            'search' => $request->query->get('search', ''),
            'public' => $request->query->get('public', 1),
            'type' => $request->query->get('type', ''),
        ];

        // Get data
        $entities = $elementRepository->findData(['order' => 'order'], $filters);

        if ($form->isSubmitted() && $form->isValid()) {
            $positions = $request->request->all('position');
            foreach ($positions as $id => $position) {
                foreach ($entities as $entity) {
                    if ($entity->getId() === $id) {
                        $entity->setPosition($position);
                        $em->persist($entity);

                        break;
                    }
                }
            }

            $em->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('content.element.sort.success')]);
            }

            $this->addFlash('success', 'content.element.sort.success');

            return $this->redirectToRoute('tigris_content_admin_element_index', ['type' => $type]);
        }

        return $this->render('@TigrisContent\admin\element\sort.html.twig', [
            'form' => $form,
            'entities' => $entities,
        ]);
    }
}
