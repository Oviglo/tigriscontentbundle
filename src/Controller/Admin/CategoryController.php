<?php

namespace Tigris\ContentBundle\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Controller\Admin\BaseController;
use Tigris\BaseBundle\Form\Type\FormActionsType;
use Tigris\BaseBundle\Traits\FormTrait;
use Tigris\ContentBundle\Entity\Category;
use Tigris\ContentBundle\Form\Type\CategoryType;
use Tigris\ContentBundle\Repository\CategoryRepository;

#[Route('/admin/content/category')]
#[IsGranted('ROLE_ADMIN')]
class CategoryController extends BaseController
{
    use FormTrait;

    public function generateBreadcrumbs(array $routes = []): void
    {
        $routes = array_merge([
            'menu.content.content' => null,
            'menu.content.categories' => ['route' => 'tigris_content_admin_category_index'],
        ], $routes);

        parent::generateBreadcrumbs($routes);
    }

    #[Route('/', methods: ['GET'])]
    public function index(): Response
    {
        $this->generateBreadcrumbs();
        $elementTypes = $this->getParameter('tigris_content.elements');

        return $this->render('@TigrisContent/admin/category/index.html.twig', [
            'types' => $elementTypes,
        ]);
    }

    #[Route('/data', options: ['expose' => 'admin'], methods: ['GET'])]
    public function list(Request $request, CategoryRepository $categoryRepository): Response
    {
        $criteria = $this->getCriteria($request);
        $criteria['public'] = $this->formatCriteria($request->query->get('public', ''), static::FORMAT_BOOLEAN_NULL);
        $criteria['type'] = $request->query->get('type', null);

        $data = $categoryRepository->findData($criteria);

        return $this->paginatorToJsonResponse($data);
    }

    #[Route('/new', options: ['expose' => 'admin'], methods: ['GET', 'POST'])]
    public function new(Request $request, TranslatorInterface $translator, EntityManagerInterface $em): Response
    {
        $entity = new Category();

        $form = $this->createForm(CategoryType::class, $entity, [
            'action' => $this->generateUrl('tigris_content_admin_category_new'),
            'method' => 'POST',
        ]);

        $this->addFormActions($form, $this->generateUrl('tigris_content_admin_category_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('content.category.add.success'), 'entity' => $entity]);
            }
            $this->addFlash('success', 'content.category.add.success');

            return $this->redirectToRoute('tigris_content_admin_category_index');
        }

        return $this->render('@TigrisContent/admin/category/new.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/{id}/edit', requirements: ['id' => '\d+'], options: ['expose' => 'admin'], methods: ['GET', 'POST'])]
    public function edit(Request $request, Category $entity, TranslatorInterface $translator, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(CategoryType::class, $entity, [
            'action' => $this->generateUrl('tigris_content_admin_category_edit', ['id' => $entity->getId()]),
            'method' => 'POST',
        ]);

        $this->addFormActions($form, $this->generateUrl('tigris_content_admin_category_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('shop.category.edit.success')]);
            }
            $this->addFlash('success', 'content.category.edit.success');

            return $this->redirectToRoute('tigris_content_admin_category_index');
        }

        return $this->render('@TigrisContent/admin/category/edit.html.twig', [
            'form' => $form,
            'entity' => $entity,
        ]);
    }

    #[Route('/{id}/remove', requirements: ['id' => '\d+'], options: ['expose' => 'admin'], methods: ['GET', 'DELETE'])]
    public function remove(Request $request, Category $entity, TranslatorInterface $translator, EntityManagerInterface $em): Response
    {
        $form = $this->getDeleteForm($entity, 'tigris_content_admin_category_');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($entity);
            $em->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('content.category.remove.success')]);
            }
            $this->addFlash('success', 'content.category.remove.success');

            return $this->redirectToRoute('tigris_content_admin_category_index');
        }

        return $this->render('@TigrisContent/admin/category/remove.html.twig', [
            'form' => $form->createView(),
            'entity' => $entity,
        ]);
    }

    #[Route('/sort', options: ['expose' => 'admin'], methods: ['GET', 'PUT'])]
    public function sort(Request $request, TranslatorInterface $translator, RouterInterface $router, CategoryRepository $categoryRepository, EntityManagerInterface $em): Response
    {
        $form = $this->createFormBuilder()
            ->setMethod(Request::METHOD_PUT)
            ->setAction($router->generate('tigris_content_admin_category_sort'))
            ->add('actions', FormActionsType::class, [
                'label' => false,
                'buttons' => [
                    'save' => [
                        'type' => SubmitType::class,
                        'options' => ['label' => 'button.save'],
                    ],
                    'cancel' => [
                        'type' => ButtonType::class,
                        'options' => [
                            'as_link' => true,
                            'label' => 'button.cancel',
                            'attr' => ['data-cancel' => true, 'href' => $this->generateUrl('tigris_content_admin_category_index')],
                        ],
                    ],
                ],
            ])
            ->getForm()
        ;

        $form->handleRequest($request);

        $entities = $categoryRepository->getTree();

        if ($form->isSubmitted() && $form->isValid()) {
            $allEntities = $categoryRepository->findAll();
            $positions = $request->request->all('position');
            // asort($positions);
            foreach ($positions as $id => $position) {
                foreach ($allEntities as $entity) {
                    if ($entity->getId() === $id) {
                        $entity->setPosition($position);

                        $em->persist($entity);

                        break;
                    }
                }
            }
            $em->flush();

            // reorder
            $categoryRepository->reorderAll('position');

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('content.category.sort.success')]);
            }

            return $this->redirectToRoute('tigris_content_admin_category_index');
        }

        return $this->render('@TigrisContent\admin\category\sort.html.twig', [
            'form' => $form,
            'entities' => $entities,
        ]);
    }
}
