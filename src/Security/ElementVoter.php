<?php

namespace Tigris\ContentBundle\Security;

use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\ContentBundle\Entity\Element;

class ElementVoter extends Voter
{
    final public const VIEW = 'view';
    final public const EDIT = 'edit';

    public function __construct(private readonly Security $security, private array $elementTypes)
    {
    }

    protected function supports(string $attribute, $subject): bool
    {
        if (!in_array($attribute, [self::VIEW, self::EDIT])) {
            return false;
        }

        return $subject instanceof Element;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::VIEW => $this->canView($subject, $user),
            self::EDIT => $this->canEdit($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    private function canView(Element $element, $user): bool
    {
        if ($this->canEdit($element, $user)) {
            return true;
        }

        return $element->isPublic() && ($this->security->isGranted('ROLE_USER') || !$element->isAccessAuthenticated());
    }

    private function canEdit(Element $element, $user): bool
    {
        if (!$user instanceof User) {
            return false;
        }

        if ($this->security->isGranted('ROLE_ADMIN') || $this->security->isGranted('ROLE_CONTENT_ELEMENT_ADMIN')) {
            return true;
        }

        if (!isset($this->elementTypes[$element->getType()])) {
            return false;
        }

        if (null === $element->getUser()) {
            return false;
        }

        return $element->getUser() === $user && $this->elementTypes[$element->getType()]['allow_user_adding'];
    }
}
