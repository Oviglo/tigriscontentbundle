<?php

namespace Tigris\ContentBundle\Security;

use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\ContentBundle\Entity\Category;

class CategoryVoter extends Voter
{
    final public const VIEW = 'view';
    final public const EDIT = 'edit';

    public function __construct(private readonly Security $security)
    {
    }

    protected function supports(string $attribute, $subject): bool
    {
        if (!in_array($attribute, [self::VIEW, self::EDIT])) {
            return false;
        }

        return $subject instanceof Category;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::VIEW => $this->canView($subject, $user),
            self::EDIT => $this->canEdit($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    private function canView(Category $subject, $user): bool
    {
        if ($this->canEdit($subject, $user)) {
            return true;
        }

        if (!$this->security->isGranted('IS_AUTHENTICATED_REMEMBERED') && $subject->isAccessAuthenticated()) {
            return false;
        }

        return $subject->isPublic();
    }

    private function canEdit(Category $subject, $user): bool
    {
        if (!$user instanceof User) {
            return false;
        }

        return $this->security->isGranted('ROLE_ADMIN') || $this->security->isGranted('ROLE_CONTENT_CATEGORY_ADMIN');
    }
}
