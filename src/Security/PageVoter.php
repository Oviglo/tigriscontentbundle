<?php

namespace Tigris\ContentBundle\Security;

use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\ContentBundle\Entity\Page;

class PageVoter extends Voter
{
    final public const VIEW = 'view';
    final public const EDIT = 'edit';

    public function __construct(private readonly Security $security)
    {
    }

    protected function supports(string $attribute, $subject): bool
    {
        if (!in_array($attribute, [self::VIEW, self::EDIT])) {
            return false;
        }

        return $subject instanceof Page;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::VIEW => $this->canView($subject, $user),
            self::EDIT => $this->canEdit($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    private function canView(Page $subject, $user): bool
    {
        if ($this->canEdit($subject, $user)) {
            return true;
        }

        if (!$this->security->isGranted('ROLE_USER') && $subject->isAccessAuthenticated()) {
            return false;
        }

        return $subject->isPublic();
    }

    private function canEdit(Page $subject, $user): bool
    {
        if (!$user instanceof User) {
            return false;
        }

        if ($this->security->isGranted('ROLE_ADMIN') || $this->security->isGranted('ROLE_CONTENT_ELEMENT_ADMIN')) {
            return true;
        }

        if (null === $subject->getUser()) {
            return false;
        }

        return $subject->getUser()->getUsername() === $user->getUsername();
    }
}
