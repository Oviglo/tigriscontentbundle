<?php

namespace Tigris\ContentBundle;

use Symfony\Component\Config\Definition\Configurator\DefinitionConfigurator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

class TigrisContentBundle extends AbstractBundle
{
    public function loadExtension(array $config, ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $parameters = $container->parameters()
            ->set('tigris_content', $config)
        ;

        foreach ($config as $key => $value) {
            $parameters->set("tigris_content.$key", $value);
        }

        $container->import(__DIR__.'/../config/services.yaml');
    }

    public function configure(DefinitionConfigurator $definition): void
    {
        $definition->rootNode()
            ->children()
                ->booleanNode('allow_translate')->defaultFalse()->end()

                ->arrayNode('menus')
                ->useAttributeAsKey('id')
                    ->arrayPrototype()
                        ->children()
                        ->scalarNode('name')->end()
                        ->end()
                    ->end()
                ->end()

            ->end()
        ;

        $definition->import(__DIR__.'/../config/elementDefinition.php');
        $definition->import(__DIR__.'/../config/pageDefinition.php');
    }
}
