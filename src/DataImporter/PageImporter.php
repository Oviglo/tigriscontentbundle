<?php

namespace Tigris\ContentBundle\DataImporter;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Tigris\BaseBundle\DataImporter\AbstractImporter;
use Tigris\ContentBundle\Entity\Page;
use Tigris\ContentBundle\Repository\PageRepository;

class PageImporter extends AbstractImporter
{
    public function __construct(TokenStorageInterface $token, EntityManagerInterface $entityManager, private readonly PageRepository $pageRepository)
    {
        parent::__construct($token, $entityManager);
    }

    private static array $pages = [];

    public function createEntity(array $data): object
    {
        $createdAt = isset($data['createdAt']) ? new \DateTime($data['createdAt']) : new \DateTime();
        $updatedAt = isset($data['updatedAt']) ? new \DateTime($data['updatedAt']) : new \DateTime();
        $public = $data['public'] ?? true;
        $menus = isset($data['menus']) ? explode(',', (string) $data['menus']) : [];
        $parent = $data['parent'];

        if (ctype_digit((string) $parent)) { // parent is integer
            $parent = (int) $parent;
        }

        if (empty($parent)) {
            $parent = null;
        }

        if (empty($data['name'])) {
            return null;
        }

        $parentEntity = null;
        if (null != $parent) {
            $pageKeys = array_keys(self::$pages);
            if (is_int($parent) && isset(self::$pages[$pageKeys[$parent]])) {
                $parentEntity = self::$pages[$pageKeys[$parent]];
            } elseif (isset(self::$pages[$parent])) {
                $parentEntity = self::$pages[$parent];
            }
        }

        $entity = (new Page())
            ->setName($data['name'])
            ->setPublic($public)
            ->setCreatedAt($createdAt)
            ->setUpdatedAt($updatedAt)
            ->setMenus($menus)
            ->setTemplate('base')
            ->setParent($parentEntity);

        self::$pages[$data['name']] = $entity;

        return $entity;
    }

    public function getEntity(string $primaryKey, mixed $primaryKeyValue, array $data): ?object
    {
        try {
            return $this->pageRepository->findOneBy([$primaryKey => $primaryKeyValue]);
        } catch (\Exception) {
            return null;
        }
    }

    public function getMapping(): array
    {
        return [
            'name' => 'string',
            'public' => 'bool',
            'createdAt' => 'datetime',
            'updatedAt' => 'datetime',
            'menus' => 'string',
            'parent' => 'string',
        ];
    }
}
