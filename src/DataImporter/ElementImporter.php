<?php

namespace Tigris\ContentBundle\DataImporter;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Tigris\BaseBundle\DataImporter\AbstractImporter;
use Tigris\BaseBundle\DataImporter\ImportOption;
use Tigris\ContentBundle\Entity\Element;
use Tigris\ContentBundle\Repository\ElementRepository;

class ElementImporter extends AbstractImporter
{
    public function __construct(private array $elementConfig, TokenStorageInterface $token, EntityManagerInterface $entityManager, private readonly ElementRepository $elementRepository)
    {
        parent::__construct($token, $entityManager);
    }

    public function createEntity(array $data): null|object
    {
        $importOptions = $this->importEntity->getOptions();
        if (!isset($importOptions['elementType']) || !isset($this->elementConfig[$importOptions['elementType']])) {
            return null;
        }

        $entity = (new Element())
            ->setType($importOptions['elementType'])
            ->setPublic($importOptions['defaultPublished'] ?? false);

        $this->hydrateEntity($entity, $data);

        return $entity;
    }

    public function getEntity(string $primaryKey, mixed $primaryKeyValue, array $data): ?object
    {
        try {
            $importOptions = $this->importEntity->getOptions();

            $entity = $this->elementRepository->findOneBy([$primaryKey => $primaryKeyValue, 'type' => $importOptions['elementType']]);

            if ($entity instanceof Element && !isset($data['public'])) {
                $entity->setPublic($importOptions['defaultPublished'] ?? false);

                $this->hydrateEntity($entity, $data);

                return $entity;
            }

            return null;
        } catch (\Exception) {
            return null;
        }
    }

    public function getMapping(): array
    {
        $map = [
            'name' => 'string',
            'public' => 'bool',
            'createdAt' => 'datetime',
            'updatedAt' => 'datetime',
            'unpublishedAt' => 'datetime',
        ];
        if (null == $this->importEntity) {
            return $map;
        }

        $importOptions = $this->importEntity->getOptions();
        if (isset($importOptions['elementType']) && isset($this->elementConfig[$importOptions['elementType']])) {
            $elementFields = $this->elementConfig[$importOptions['elementType']]['fields'];
            foreach ($elementFields as $name => $field) {
                if (in_array($field['type'], ['wysiwyg', 'text', 'email', 'url', 'textarea'])) {
                    $map[$name] = 'string';
                }
            }
        }

        return $map;
    }

    public function getOptions(): array
    {
        return [
            new ImportOption('replace', CheckboxType::class, [
                'label' => 'data.import.options.replace',
                'required' => false,
            ]),
        ];
    }
}
