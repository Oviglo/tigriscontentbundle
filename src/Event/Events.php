<?php

namespace Tigris\ContentBundle\Event;

class Events
{
    final const LOAD_CONTENT_MENU = 'tigris_content.load_content_menu';
    final const LOAD_CONTENT_CATEGORY_MENU = 'tigris_content.load_content_category_menu';

    final const CONTENT_ELEMENT_UPDATED = 'tigris_content.content_element_updated';
    final const CONTENT_ELEMENT_ADDED = 'tigris_content.content_element_added';
    final const CONTENT_ELEMENT_DELETED = 'tigris_content.content_element_deleted';
    final const FIND_ELEMENT_BY_TYPE = 'tigris_content.find_element_by_type';

    final const SHOW_PAGE = 'tigris_content.show_page';

    final const SHOW_ELEMENT = 'tigris_content.show_element';
}
