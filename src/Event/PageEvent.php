<?php

namespace Tigris\ContentBundle\Event;

use Tigris\BaseBundle\Event\ViewEvent;
use Tigris\ContentBundle\Entity\Page;

class PageEvent extends ViewEvent
{
    public function __construct(private readonly Page $page)
    {
    }

    public function getPage(): Page
    {
        return $this->page;
    }
}
