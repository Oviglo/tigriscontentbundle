<?php

namespace Tigris\ContentBundle\Event;

use Tigris\BaseBundle\Event\ViewEvent;
use Tigris\ContentBundle\Entity\Element;

class ElementEvent extends ViewEvent
{
    public function __construct(private Element $element)
    {
    }

    public function getElement(): Element
    {
        return $this->element;
    }

    public function setElement(Element $element): self
    {
        $this->element = $element;

        return $this;
    }
}
