<?php

namespace Tigris\ContentBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Tigris\BaseBundle\Event\MenuEvent;
use Tigris\ContentBundle\Entity\Page;
use Tigris\ContentBundle\Event\Events;
use Tigris\ContentBundle\Repository\CategoryRepository;
use Tigris\ContentBundle\Repository\PageRepository;

class MenuBuilder
{
    /**
     * Add any other dependency you need.
     */
    public function __construct(private readonly FactoryInterface $factory, private readonly EventDispatcherInterface $eventDispatcher, private readonly PageRepository $pageRepository, private readonly CategoryRepository $categoryRepository, private readonly Security $security)
    {
    }

    public function createPageMenu(array $options)
    {
        $menu = $this->factory->createItem('root');
        $name = $options['name'] ?? '';
        $pages = $this->pageRepository->getMenuTree($name);
        $this->addPageTree($pages, $menu);
        $event = new MenuEvent($menu, ['type' => 'page', 'name' => $name]);
        $this->eventDispatcher->dispatch($event, Events::LOAD_CONTENT_MENU);

        return $event->getMenu();
    }

    private function addPageTree($pages, $menu)
    {
        foreach ($pages as $page) {
            if (!$page->isPublic() || !$this->security->isGranted('view', $page)) {
                continue;
            }
            $item = null;

            switch ($page->getType()) {
                case Page::TYPE_TEXT:
                    $item = $menu->addChild($page->getNamePrefix().$page->getName(), [
                        'route' => 'tigris_content_page_show',
                        'routeParameters' => ['slug' => $page->getSlug()],
                        'attributes' => [
                            'class' => ($page->isAccessAuthenticated() ? 'access-authenticated' : ''),
                        ],
                    ])->setExtra('translation_domain', false);
                    break;

                case Page::TYPE_URL:
                    $item = $menu->addChild($page->getNamePrefix().$page->getName(), [
                        'uri' => $page->getUrl(),
                        'attributes' => [
                            'class' => ($page->isAccessAuthenticated() ? 'access-authenticated' : ''),
                        ],
                    ])->setExtra('translation_domain', false);
                    break;

                case Page::TYPE_CONTENT_LIST:
                    $item = $menu->addChild($page->getNamePrefix().$page->getName(), [
                        'route' => 'tigris_content_element_list',
                        'routeParameters' => ['type' => $page->getContentType()],
                        'attributes' => [
                            'class' => ($page->isAccessAuthenticated() ? 'access-authenticated' : ''),
                        ],
                    ])->setExtra('translation_domain', false);
                    break;
            }

            if ((is_countable($page->getChildren()) ? count($page->getChildren()) : 0) && null != $item) {
                $this->addPageTree($page->getChildren(), $item);
            }
        }
    }

    public function createCategoryMenu(array $options)
    {
        $menu = $this->factory->createItem('root');
        $type = $options['type'] ?? null;
        $categories = $this->categoryRepository->getMenuTree($type);
        $this->addCategoryTree($categories, $menu);
        $event = new MenuEvent($menu, ['type' => 'category']);
        $this->eventDispatcher->dispatch($event, Events::LOAD_CONTENT_CATEGORY_MENU);

        return $event->getMenu();
    }

    private function addCategoryTree($categories, $menu)
    {
        foreach ($categories as $key => $category) {
            if (!$this->security->isGranted('view', $category)) {
                continue;
            }
            $item = $menu->addChild($category->getNamePrefix().$category->getName(), [
                'route' => 'tigris_content_category_show',
                'routeParameters' => [
                    'slug' => $category->getSlug(),
                ],
                'attributes' => [
                    'class' => ($category->isAccessAuthenticated() ? 'access-authenticated' : ''),
                ],
            ])->setExtra('translation_domain', false);
            if ((is_countable($category->getChildren()) ? count($category->getChildren()) : 0) > 0) {
                $this->addCategoryTree($category->getChildren(), $item);
            }
        }
    }
}
